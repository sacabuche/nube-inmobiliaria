from django.conf.urls.defaults import patterns, url
from properties.views import (form_view, create_property_view,
                              property_detail_view, property_edit_view,
                              create_image_view, search_view,
                              set_favourite_view, delete_image_view,
                              set_main_picture_view, custom_search_form_view,
                              delete_property_view)


urlpatterns = patterns('',
    url(r'^crear/$', create_property_view, name='property-create'),
    url(r'^buscar/$', search_view, name='property-search'),
    url(r'^formulario/$', form_view, name='property-form'),
    url(r'^(?P<pk>\d+)/$', property_detail_view, name='property-detail'),
    url(r'^(?P<pk>\d+)/editar/$', property_edit_view, name='property-edit'),
    url(r'^(?P<pk>\d+)/imagen-nueva/$', create_image_view,
        name='property-add-image'),
    url(r'^(?P<pk>\d+)/borrar-imagen/(?P<image_id>\d+)$', delete_image_view,
        name='property-delete-image'),
    url(r'^(?P<pk>\d+)/poner-imagen/(?P<image_id>\d+)$', set_main_picture_view,
        name='property-set-main-picture'),
    url(r'^set-favorite/(?P<pk>\d+)/$', set_favourite_view,
        name='properties-set-favourite'),
    url(r'^borrar-inmueble/(?P<pk>\d+)/$', delete_property_view,
        name='properties-delete'),
    url(r'^custom-search-form/$', custom_search_form_view,
        name='properties-custom-form'),
)
