$(function () {
    var $photos_box = $('#fotos_box'),
    DATA_MAIN_PICTURE = 'data-main-picture',
    CLS_MAIN_PICTURE = 'main-picture';
    var photos = 0;

    /* Uploading Files */
    $('#id_imagen').fileupload({
        dataType: 'json',
        add: function (e, data) {
            $('#image-message').html('Comenzando...');
            $(this).prop('disabled', true);
            data.submit();
        },
        done: function (e, data) {
            $(this).prop('disabled', false);
            if (data.result.error) {
                $('#image-message')
                .html(data.result.error)
                .addClass('error')
                .removeClass('exito');
            }
            else {

                $('#image-message').removeClass('error').addClass('exito');
                var img = $('<img>').attr('src', data.result.url),
                span = $('<span class="image-edit">'),
                anchor = $('<a class="delete-image">').attr('href', data.result.delete_url).text('borrar');

                img.attr(DATA_MAIN_PICTURE, data.result.set_main_picture_url);
                img.attr("id", "image-id-" + data.result.id);

                $(span).append($(img)).append($(anchor));
                $photos_box.append(span);
                $('#image-message').show().text("Foto Cargada").fadeOut(5000);
            }
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10),
            text = 'Cargando Foto... ' + progress + '%s';
            if (progress === 100) {
                text = 'Processando Imagen...';
            }
            $('#image-message').show().text(text);
        }

    });

    var clear_main_picture = function () {
        $photos_box.find('.' + CLS_MAIN_PICTURE).removeClass(CLS_MAIN_PICTURE);
    };

    var set_main_picture = function (id) {
        clear_main_picture();
        $('#image-id-' + id).addClass(CLS_MAIN_PICTURE);
    };

    /* DELETING IMAGE  */
    $photos_box.on('click', '.delete-image', function (e) {
        e.preventDefault();
        var $target = $(e.target);
        $.post($target.attr('href'), function (data) {
            $target.parents('.image-edit').remove();
            set_main_picture(data.main_picture_id);
        });
    });

    /* SELECTING MAIN PICTURE */
    $photos_box.on('click', 'img', function (e) {
        var $target = $(e.target);
        $.post($target.attr(DATA_MAIN_PICTURE), function (data) {
            clear_main_picture();
            $target.addClass(CLS_MAIN_PICTURE);
        });
    });


});
