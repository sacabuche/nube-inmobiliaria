$(document).ready(function () {
    var $main_form = $('#main_form'),
    $main_form_content = $main_form.find('#form_content'),
    $type_input = $('#id_tipo'),
    $input_hidden = $('#property-type'),
    $create_button = $('#create-button');

    if (!$input_hidden.val()) {
        $create_button.hide();
    }

    function add_cp_message() {
        $('#id_codigo_postal').parent().append('<div id="cp-message"></div>');
    }

    function get_form(type) {
        $main_form_content.text('');
        $create_button.hide();
        $input_hidden.val(type);
        $.get(get_form_url, {'property-type': type}, function (data) {
            var $data = $(data),  //.filter('p'),
            is_even = true,
            cls, $div,
            all = [];
            $main_form_content.append($data);
            add_cp_message();
            $create_button.show();
        });
    }

    function on_select_form(e) {
        var html = '',
        type = $(this).val();

        if (!type) {
            return;
        }
        get_form(type);
    }

    function set_colonias(data) {
        var colonias = data.objects,
        $estado = $('#id_estado'),
        $municipio = $('#id_municipio'),
        $colonia_select = $('#id_colonia'),
        colonias_op = [],
        colonia,
        option,
        $option_obj = $('<option>'),
        $message = $('#cp-message');

        /* clean inputs */
        $estado.html('');
        $municipio.html('');
        $colonia_select.html('');

        /* Get Municipality Name and state name */
        if (colonias.length) {
            $message.html('');
            colonia = colonias[0];
            $.get(colonia.municipality, function (municipality) {
                $municipio.val(municipality.name);
                $.get(municipality.state, function (state) {
                    $estado.val(state.name);
                });
            });
        }
        else {
            $message.html('No existe este código postal');
            return;
        }

        for (var i = 0; i < colonias.length; i++) {
            colonia = colonias[i];
            option = $option_obj.clone();
            option.val(colonia.id);
            option.html(colonia.name);
            colonias_op.push(option);
        }

        $colonia_select.append(colonias_op);
    }

    function read_CP(e) {
        var $target = $(e.target),
        value = $target.val(),
        $estado = $('#id_estado'),
        $municipio = $('#id_municipio'),
        $colonia_select = $('#id_colonia'),

        $message = $('#cp-message').html('');
        if (!$.isNumeric(value)) {
            $message.html('Error: Solo acepta dígitos');
            value = value.slice(0, value.length - 1);
            $target.val(value);
            return;
        }

        if (value.length < 5) {
            $estado.val('');
            $municipio.val('');
            $colonia_select.html('');
            $message.html('Tienen que ser 5 dígitos');
            return;
        }

        $message.html('buscando...');
        $.ajax({
            method: 'get',
            url: '/api/sepomex/v1/settlement/',
            data: {postal_code__number: value},
            success: set_colonias
        });
    }


    $type_input.change(on_select_form);
    $('#main_form').on('keyup', '#id_codigo_postal', read_CP);
    add_cp_message();

    $('#delete-button').click(function (e) {
        var submit = confirm("¿Realmente quiere borrar la propiedad?");
        if (!submit) {
            e.preventDefault();
        }
    });


    $('.errorlist')
    .hide()
    .next()
    .find('label')
    .css('color', 'red');
});
