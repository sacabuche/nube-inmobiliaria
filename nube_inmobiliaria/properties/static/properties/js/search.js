$(document).ready(function () {
    var form_class = '.advanced-search';

    function init_events() {
        $('#id_tipo_de_inmueble').change(function (e) {
            get_menu($(e.target).val());
        });

        $('.clean-button').click(function (e) {
            e.preventDefault();
            window.location = '?limpiar';
        });
    }

    function get_menu(type) {
        $.get(window.menu_search_url, {
            'property-type': type
        }, load_menu);
    }

    function load_menu(data) {
        $('#advanced-search').html(data);
        if (data === '') {
            $(form_class).hide();
        }
        else {
            $(form_class).show();
        }
    }

    function menu_has_content() {
        return $('#advanced-search').find('tr').size() > 0;
    }



    init_events();
    if (!menu_has_content()) {
        $(form_class).hide();
    }
});
