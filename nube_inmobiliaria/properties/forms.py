# coding: utf-8
from django import forms
from django.utils.html import conditional_escape  # format_html
from django.utils.encoding import force_unicode
from django.utils.safestring import mark_safe
from django.db.models import Q, BooleanField, CharField, IntegerField

from sepomex.models import Settlement, State
from currencies.models import Currency
from ads.models import Ad
from properties.models import TIPOS_DE_INMUEBLE, Inmueble, TYPE_CLASS
from properties.models import (Terreno, Oficina, CasaComercialOficina,
                               CasaSola, CasaEnPrivada, CasaEnCondominio,
                               Departamento, Loft, EdificioOficinas,
                               LocalComercial, Bodega, NaveIndustrial,
                               Imagen)

TIPOS = [('', '---------')]
TIPOS.extend(sorted(TIPOS_DE_INMUEBLE.items()))


class ImagenForm(forms.ModelForm):
    class Meta:
        model = Imagen


class UserOrderedMixin(object):

    def _html_formated_output(self, formated_fields, normal_row, error_row,
                              row_ender, help_text_html,
                              errors_on_separate_row):
        "Helper function for outputting HTML."
        "Used by as_table(), as_ul(), as_p()."
        # Errors that should be displayed above all fields."
        top_errors = self.non_field_errors()
        output, hidden_fields = [], []
        the_fields = self.fields

        for name in formated_fields:
            try:
                field = the_fields[name]
            except KeyError:
                continue
            html_class_attr = ''
            bf = self[name]
            # Escape and cache in local variable.
            bf_errors = self.error_class([conditional_escape(error)
                                          for error in bf.errors])
            if bf.is_hidden:
                if bf_errors:
                    top_errors.extend([u'(Hidden field %s)'
                                       ' %s' % (name, force_unicode(e))
                                       for e in bf_errors])
                hidden_fields.append(unicode(bf))
            else:
                # Create a 'class="..."' atribute if the row should have any
                # CSS classes applied.
                css_classes = bf.css_classes()
                if css_classes:
                    html_class_attr = ' class="%s"' % css_classes

                if errors_on_separate_row and bf_errors:
                    output.append(error_row % force_unicode(bf_errors))

                if bf.label:
                    label = conditional_escape(force_unicode(bf.label))
                    # Only add the suffix if the label does not end in
                    # punctuation.
                    if self.label_suffix:
                        if label[-1] not in ':?.!':
                            label += self.label_suffix
                    label = bf.label_tag(label) or ''
                else:
                    label = ''

                if field.help_text:
                    help_text = help_text_html % force_unicode(field.help_text)
                else:
                    help_text = u''

                output.append(normal_row % {
                    'errors': force_unicode(bf_errors),
                    'label': force_unicode(label),
                    'field': unicode(bf),
                    'help_text': help_text,
                    'html_class_attr': html_class_attr
                })

        if top_errors:
            output.insert(0, error_row % force_unicode(top_errors))

        # Insert any hidden fields in the last row.
        if hidden_fields:
            str_hidden = u''.join(hidden_fields)
            if output:
                last_row = output[-1]
                # Chop off the trailing row_ender (e.g. '</td></tr>') and
                # insert the hidden fields.
                if not last_row.endswith(row_ender):
                    # This can happen in the as_p() case (and possibly others
                    # that users write): if there are only top errors, we may
                    # not be able to conscript the last row for our purposes,
                    # so insert a new, empty row.
                    last_row = (normal_row % {
                        'errors': '', 'label': '',
                        'field': '', 'help_text': '',
                        'html_class_attr': html_class_attr
                    })
                    output.append(last_row)
                output[-1] = last_row[:-len(row_ender)] \
                    + str_hidden + row_ender
            else:
                # If there aren't any rows in the output, just append the
                # hidden fields.
                output.append(str_hidden)
        return mark_safe(u'\n'.join(output))

    @classmethod
    def get_formated_fields(cls):
        classes = cls.mro()
        ordering = []
        for cls_ in classes:
            try:
                ordering.append(cls_.Meta.formated_fields)
            except AttributeError:
                pass
        return ordering

    def as_formated_div(self):
        output = []
        all_fields = set(self.fields.keys())
        added_fields = set()
        clear_field = '<div class="clear"></div>'
        hr = '<hr/>'
        for cls_fields in self.get_formated_fields():
            for group in cls_fields:
                group = dict(group)
                title = group.pop('_title', None)
                if title:
                    output.append('<h2 class="formated-title">'
                                  ' %s </h2>' % force_unicode(title))
                order = group.pop('_order', group.keys())
                for html_class in order:
                    fields = group[html_class]
                    added_fields.update(fields)
                    output.append(self._group_wrapper(html_class, fields))
                output.append(clear_field + hr)

        no_formated = all_fields - added_fields
        if len(no_formated):
            output.append(self._group_wrapper('left no-formated-fields',
                                              no_formated))
            output.append(clear_field)
        return mark_safe(u'\n\n'.join(output))

    def _group_wrapper(self, html_class, fields):
        html_fields = self._html_formated_output(
            fields,
            normal_row='<p%(html_class_attr)s>%(label)s '
                       '%(field)s%(help_text)s</p>',
            error_row='%s',
            row_ender='</p>',
            help_text_html=' <span class="helptext">%s</span>',
            errors_on_separate_row=True)

        return '<div class="%s">%s</div>' % (html_class, html_fields)


class InmuebleForm(UserOrderedMixin, forms.ModelForm):

    estado = forms.CharField(widget=forms.TextInput, required=False)
    codigo_postal = forms.CharField(widget=forms.TextInput, max_length=5)
    municipio = forms.CharField('municipio/delegación', widget=forms.TextInput,
                                required=False)

    disable_attrs = dict(readonly='readonly')

    class Meta:
        formated_fields = (
            {
                '_title': u'Selecione el Anuncio',
                'ad': ('ad',),
            },

            {
                '_title': u'Dirección',
                '_order': ('left', 'right', 'clear',
                           'someextrapadding calle-ref'),
                'left': ('codigo_postal',),
                'right': ('estado', 'municipio', 'colonia'),
                'clear': ('',),
                'someextrapadding calle-ref': ('calle_y_num', 'referencias',),
            },

            {
                '_title': u'Comisiones',
                '_order': ('left', 'right', 'clear',),
                'left': ('pct_pactada',),
                'right': ('pct_compartir',),
                'clear': ('',),
            },

            {
                '_title': u'Información Básica',
                '_order': ('left', 'right', 'clear', 'someextrapadding'),
                'left': Inmueble._general_fields_free,
                'right': Inmueble._general_fields_choices,
                'clear': ('',),
                'someextrapadding': ('descripcion',
                                     'observaciones',),
            },

            {
                '_title': 'Extras del Inmueble',
                '_order': ('left', 'right', 'clear'),
                'left': Inmueble._extras_left,
                'right': Inmueble._extras_right,
                'clear': ('',),
            }

        )

    def set_address(self):
        self.fields.pop('imagenes', None)
        instance = self.instance
        colonia = self.data.get('colonia', False)
        if instance.id or colonia:
            if colonia:
                settlement = Settlement.objects.get(id=colonia)
            else:
                settlement = instance.colonia
            municipality = settlement.municipality
            state = municipality.state

            self.fields['estado'].initial = state.name
            self.fields['municipio'].initial = municipality.name
            self.fields['codigo_postal'].initial = str(settlement.postal_code)

            colonias = Settlement.objects\
                .filter(postal_code=settlement.postal_code)
            self.fields['colonia'].queryset = colonias
        else:
            self.fields['colonia'].widget = forms.Select()

        self.fields['estado'].widget.attrs.update(self.disable_attrs)
        self.fields['municipio'].widget.attrs.update(self.disable_attrs)

    def set_ad(self, user):
        instance = self.instance
        queryset = Ad.objects.filter(user=user)
        if instance.id and instance.ad:
            queryset = queryset\
                .filter(Q(status=Ad.READY) | Q(id=instance.ad.id))
            self.fields['ad'].initial = instance.ad
        else:
            queryset = queryset.filter(status=Ad.READY)
            self.fields['ad'].queryset = queryset

    def __init__(self, user, *args, **kwargs):
        super(InmuebleForm, self).__init__(*args, **kwargs)
        self.set_address()
        self.set_ad(user)


class TerrenoForm(InmuebleForm, forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(TerrenoForm, self).__init__(*args, **kwargs)
        field = self.fields['tipo']
        field.widget = forms.HiddenInput(attrs={'value': self.tipo_de_terreno})

    class Meta:
        model = Terreno


class TerrenoHabitacionalForm(TerrenoForm):
    tipo_de_terreno = Terreno.HABITACIONAL


class TerrenoComercialForm(TerrenoForm):
    tipo_de_terreno = Terreno.COMERCIAL


class TerrenoCondominioHabForm(TerrenoForm):
    tipo_de_terreno = Terreno.CONDOMINIO


class TerrenoIndustrialForm(TerrenoForm):
    tipo_de_terreno = Terreno.INDUSTRIAL


class CondominioMixin(object):
    class Meta:
        formated_fields = (
            {
                '_title': 'Servicios del condominio',
                'left': Inmueble._condominio_left,
                'right': Inmueble._condominio_right,
            },
        )


class AdicionalesMixin(object):
    class Meta:
        formated_fields = (
            {
                '_title': 'Adicionales Importantes',
                'left': Inmueble._adicionales_left,
                'right': Inmueble._adicionales_right,
            },
        )


class DepartamentoForm(InmuebleForm, CondominioMixin,
                       AdicionalesMixin):
    class Meta:
        model = Departamento


class LoftForm(InmuebleForm, CondominioMixin,
               AdicionalesMixin):
    class Meta:
        model = Loft


class CasaEnCondominioForm(InmuebleForm, CondominioMixin,
                           AdicionalesMixin):
    class Meta:
        model = CasaEnCondominio


class CasaSolaForm(InmuebleForm,
                   AdicionalesMixin):
    class Meta:
        model = CasaSola


class CasaEnPrivadaForm(InmuebleForm,
                        AdicionalesMixin):
    class Meta:
        model = CasaEnPrivada


class CasaComercialOficinaForm(InmuebleForm,
                               AdicionalesMixin):
    class Meta:
        model = CasaComercialOficina


class OficinaForm(InmuebleForm,
                  AdicionalesMixin):
    class Meta:
        model = Oficina


class EdificioOficinasForm(InmuebleForm,
                           AdicionalesMixin):
    class Meta:
        model = EdificioOficinas


class LocalComercialForm(InmuebleForm,
                         AdicionalesMixin):
    class Meta:
        model = LocalComercial


class BodegaForm(InmuebleForm):
    class Meta:
        model = Bodega


class NaveIndustrialForm(InmuebleForm):
    class Meta:
        model = NaveIndustrial


class PropertyCreateForm(forms.Form):
    tipo = forms.ChoiceField(choices=TIPOS)

    class Media:
        js = ('properties/js/properties.js',)


FORMS_BY_TYPE = {
    u'DEP': DepartamentoForm,
    u'LFT': LoftForm,
    u'CSL': CasaSolaForm,
    u'CCD': CasaEnCondominioForm,
    u'CPV': CasaEnPrivadaForm,
    u'CCO': CasaComercialOficinaForm,
    u'THB': TerrenoHabitacionalForm,
    u'TCM': TerrenoComercialForm,
    u'TCH': TerrenoCondominioHabForm,
    u'TID': TerrenoIndustrialForm,
    u'OFN': OficinaForm,
    u'EOF': EdificioOficinasForm,
    u'LCM': LocalComercialForm,
    u'BDG': BodegaForm,
    u'NIN': NaveIndustrialForm,
}


def cp_and_null(choices):
    cp_choices = [('', '--------')]
    return cp_choices + list(choices)


class SearchForm(forms.Form):
    OPERACIONES = cp_and_null(Inmueble.TIPOS_DE_TRANSACCION)
    ESTADOS = cp_and_null([(s.id, s.name) for s in State.objects.all()])
    DIVISAS = [(d.id, d.code) for d in Currency.objects.all()]

    referencia = forms.IntegerField(required=False)
    tipo_de_inmueble = forms.ChoiceField(required=False, choices=TIPOS)
    operacion = forms.ChoiceField(required=False, choices=OPERACIONES)
    estado = forms.ChoiceField(required=False, choices=ESTADOS)
    codigo_postal = forms.CharField(required=False, max_length=5, min_length=5)
    colonia = forms.CharField(required=False)
    superficie_minima = forms.IntegerField(label='superficie mín (m²)',
                                           required=False)
    superficie_maxima = forms.IntegerField(label='superficie máx (m²)',
                                           required=False)
    divisa = forms.ChoiceField(choices=DIVISAS, required=False)
    precio_minimo = forms.DecimalField(label='precio mínimo', required=False)
    precio_maximo = forms.DecimalField(label='precio máximo', required=False)


FIELDS = {
    BooleanField: forms.BooleanField,
    CharField: forms.CharField,
    IntegerField: forms.IntegerField,
}


class BaseAdvancedForm(forms.Form):

    def queryset(self):
        Model = self._model
        queryset = Model.objects.filter()
        kwargs = self.build_kwargs()
        if len(kwargs):
            return queryset.filter(**kwargs)
        return queryset

    def build_kwargs(self):
        query_kwargs = {}
        for name, value in self.cleaned_data.items():
            # Remove False because we want to show us
            # all the properties even if it has True on this
            # boolean field
            # It will filter the properties when the boolean
            # field is True
            if value in (None, False, 'None', ''):
                continue
            if name.startswith('max_'):
                name = name.replace('max_', '', True)
                sufix = '__lte'
            elif name.startswith('min_'):
                name = name.replace('min_', '', True)
                sufix = '__gte'
            else:
                sufix = ''
            query_kwargs[name + sufix] = value
        print query_kwargs
        return query_kwargs


EXCLUDE_FIELDS = ('type', 'transaccion',)


def create_form_str(model_type):
    model = TYPE_CLASS[model_type]
    return create_form_cls(model)


# XXX: Pass this a meta class??
def create_form_cls(model):
    fields = model._meta.fields
    valid_fields = set(Inmueble._all_fields) - set(EXCLUDE_FIELDS)
    form_fields = {'_model': model}
    for field in fields:
        if not field.name in valid_fields:
            continue
        try:
            FormField = FIELDS[type(field)]
        except KeyError:
            continue
        if len(field.choices):
            form_field = forms.ChoiceField(choices=[(None, "----")] +
                                           list(field.choices), required=False)
        else:
            form_field = FormField(required=False)
        if isinstance(field, IntegerField):
            form_fields['min_' + field.name] = form_field
            form_fields['max_' + field.name] = form_field
        else:
            form_fields[field.name] = form_field
    Form = type('AdvancedForm', (BaseAdvancedForm,), form_fields)
    return Form
