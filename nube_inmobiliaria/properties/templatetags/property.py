# coding: utf-8
from django import template
from django.template import Context, loader
from django.contrib.humanize.templatetags.humanize import intcomma

register = template.Library()


def _money(value):
    return '$ ' + intcomma(value)


FILTERS = {
    'precio': _money,
}


FILTER_TYPE = {
    bool: lambda value: u'sí' if value else 'no'
}


def apply_filter(field, value):
    try:
        val = FILTERS[field.name](value)
    except KeyError:
        val = value

    try:
        return FILTER_TYPE[type(val)](val)
    except KeyError:
        return val


@register.simple_tag
def services(property, services, template, exclude=''):
    result = []
    exclude_services = exclude.replace(' ', '').split(',')
    t = loader.get_template(template)
    for service in services:
        if service.name in exclude_services:
            continue
        if len(service.choices):
            value = getattr(property, 'get_' + service.name + '_display')()
        else:
            value = getattr(property, service.name)
        c = Context({
            'field_name': service.name,
            'verbose_name': service.verbose_name,
            'value': apply_filter(service, value),
        })
        result.append(t.render(c))
    return '\n'.join(result)


@register.simple_tag
def extra_services(property, template):
    result = []
    t = loader.get_template(template)
    services = property.get_extra_fields()
    for service in services:
        c = Context({
            'verbose_name': service.verbose_name,
            'field_name': service.name,
        })
        result.append(t.render(c))
    return u'\n'.join(result)
