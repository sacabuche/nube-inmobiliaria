# coding: utf-8
import json

from django.views.generic import TemplateView, View, DetailView, UpdateView
from django.views.generic import ListView
from django.http import HttpResponse, Http404
from django.shortcuts import redirect, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.contrib import messages

from django_xhtml2pdf.utils import render_to_pdf_response
from sorl.thumbnail import get_thumbnail
from ads.models import Ad
from currencies.models import Currency
from properties.models import Inmueble, Imagen, TYPE_CLASS
from properties.forms import (FORMS_BY_TYPE, PropertyCreateForm, ImagenForm,
                              SearchForm, create_form_cls, create_form_str)


class GetFormMixin(object):

    def dispatch(self, request, *args, **kwargs):
        type_ = request.GET.get('property-type',
                                request.POST.get('property-type', None))
        self.property_type = type_
        return super(GetFormMixin, self).dispatch(request, *args, **kwargs)

    def get_form_cls(self):
        try:
            return FORMS_BY_TYPE[self.property_type]
        except KeyError:
            raise Http404

    def set_form(self, *args, **kwargs):
        cls = self.get_form_cls()
        self.property_form = cls(*args, **kwargs)


class CreatePropertyView(GetFormMixin, TemplateView):
    template_name = 'properties/create.html'

    def dispatch(self, request, *args, **kwargs):
        if not request.user.ad_set.filter(status=Ad.READY).count():
            messages.add_message(request, messages.WARNING,
                                 u'No tíenes anuncios restantes, compra uno')
            return redirect('profile-buy')
        return super(CreatePropertyView,
                     self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        if self.property_type and self.request.method.lower() == 'get':
            self.set_form(self.request.user)
        context = super(CreatePropertyView, self).get_context_data(**kwargs)
        try:
            context['property_form'] = self.property_form
            context['property_type'] = self.property_type
            initial = {'tipo': self.property_type}
        except AttributeError:
            initial = {}
        context['form'] = PropertyCreateForm(initial=initial)
        return context

    def post(self, request):
        self.set_form(request.user, request.POST)
        form = self.property_form
        if form.is_valid():
            instance = form.save()
            instance.user.add(request.user)
            return redirect('property-edit', instance.id)
        return self.get(request)


class PropertyEditView(UpdateView):
    model = Inmueble
    template_name = 'properties/edit.html'

    def get_object(self):
        obj = super(PropertyEditView, self).get_object()
        if obj.owner != self.request.user:
            raise PermissionDenied()
        return obj.as_leaf_class()

    def get_form_class(self):
        try:
            return FORMS_BY_TYPE[self.object.type]
        except KeyError:
            raise Http404

    def get_form_kwargs(self):
        kwargs = super(PropertyEditView, self).get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs

    def get_success_url(self):
        return self.object.get_absolute_url()

    def get_context_data(self, **kwargs):
        context = super(PropertyEditView, self).get_context_data(**kwargs)
        context['image_form'] = ImagenForm()
        return context


class CreateImageView(View):

    IMAGE_LIMIT = 10

    def post(self, request, pk):
        inmueble = get_object_or_404(Inmueble, pk=pk)
        if inmueble.owner != request.user:
            raise Http404
        form = ImagenForm(request.POST, request.FILES)
        reach_image_limit = inmueble.imagenes.all().count() >= self.IMAGE_LIMIT
        if not(reach_image_limit) and form.is_valid():
            imagen = form.save()
            inmueble.imagenes.add(imagen)
            if not inmueble.main_picture:
                inmueble.main_picture = imagen
                inmueble.save()
            im = get_thumbnail(imagen.imagen, imagen.EDIT_SIZE, crop='center',
                               upscale=True)
            args = [inmueble.id, imagen.id]
            delete_url = reverse('property-delete-image', args=args)
            set_main_picture_url = reverse('property-set-main-picture',
                                           args=args)
            result = dict(url=im.url,
                          delete_url=delete_url,
                          set_main_picture_url=set_main_picture_url,
                          id=imagen.id)
        else:
            if reach_image_limit:
                error = 'Lo sentimos, %s imágenes es el '\
                        'límite' % self.IMAGE_LIMIT
            else:
                error = "Imágen Inválida"
            result = dict(error=error)
        return HttpResponse(json.dumps(result), mimetype="application/json")


class DeleteImageView(View):

    def get(self, *args, **kwargs):
        return self.post(*args, **kwargs)

    def post(self, request, pk, image_id):
        inmueble = get_object_or_404(Inmueble, pk=pk)
        if inmueble.owner != request.user:
            raise Http404
        image = get_object_or_404(Imagen, pk=image_id)
        if image == inmueble.main_picture:
            inmueble.imagenes.remove(image)
            try:
                inmueble.main_picture = inmueble.imagenes.all()[0]
            except IndexError:
                inmueble.main_picture = None
            inmueble.save()
        main_picture = inmueble.main_picture
        result = {'main_picture_id': main_picture and main_picture.id}
        image.delete()
        return HttpResponse(json.dumps(result), mimetype="application/json")


class DeletePropertyView(View):

    def post(self, request, pk):
        inmueble = get_object_or_404(Inmueble, pk=pk)
        if inmueble.owner != request.user:
            raise Http404
        inmueble = inmueble.leaf
        inmueble.delete()
        return redirect('profile-properties')


class SetMainPictureView(View):

    def post(self, request, pk, image_id):
        inmueble = get_object_or_404(Inmueble, pk=pk)
        if inmueble.owner != request.user:
            raise Http404
        image = get_object_or_404(Imagen, pk=image_id)
        inmueble.main_picture = image
        inmueble.save()
        return HttpResponse("OK")


class FormView(GetFormMixin, View):

    def get(self, request):
        form_cls = self.get_form_cls()
        return HttpResponse(form_cls(request.user).as_formated_div())


class SetFavouriteView(View):

    def post(self, request, pk):
        inmueble = get_object_or_404(Inmueble, pk=pk)
        user = request.user
        if user.favourites.filter(id=pk).exists():
            user.favourites.remove(inmueble)
            message = "Removed"
        else:
            user.favourites.add(inmueble)
            message = "Added"
        return HttpResponse(message)


class PropertyDetailView(DetailView):
    template_name = 'properties/detail.html'
    popup_template = 'properties/detail_popup.html'
    pdf_template = 'properties/detail_pdf.html'
    model = Inmueble

    def get_object(self):
        obj = super(PropertyDetailView, self).get_object()
        if obj.owner != self.request.user:
            obj.ad.viewed()
        return obj.as_leaf_class()

    def get_context_data(self, **kwargs):
        context = super(PropertyDetailView, self).get_context_data(**kwargs)
        if self.request.GET.get('popup', False):
            self.template_name = self.popup_template
        obj = self.object
        user = self.request.user
        context['images'] = obj.imagenes.all()
        context['image_size'] = "700x690"
        context['thmb_size'] = "140x80"
        context['owner'] = obj.owner
        context['is_favourite'] = obj.is_favourite_of(user)
        return context

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        context = self.get_context_data(object=self.object)
        if self.request.GET.get('pdf', False):
            return render_to_pdf_response(
                self.pdf_template,
                context=context,
                pdfname='Inmueble_%s.pdf' % self.object.id)
        return self.render_to_response(context)


class UserPropertiesView(ListView):
    template_name = 'properties/profile_list.html'
    model = Inmueble

    def get_queryset(self):
        return Inmueble.full_objects.filter(user=self.request.user)

    def get_context_data(self, **kwargs):
        context = super(UserPropertiesView, self).get_context_data(**kwargs)
        context['image_size'] = Imagen.PROFILE_LIST_SIZE
        return context


class CustomSearchFormView(View):

    def get(self, request):
        type_ = request.GET.get('property-type',
                                request.POST.get('property-type', None))
        try:
            cls = TYPE_CLASS[type_]
        except KeyError:
            return HttpResponse('')

        Form = create_form_cls(cls)
        form = Form(prefix='advanced')
        return HttpResponse(form.as_table())


class SearchView(ListView):
    template_name = 'properties/search.html'
    paginate_by = 3 * 3

    def post(self, request):
        self.to_search = request.POST
        return self.get(request)

    def get(self, request):
        try:
            self.to_search
        except AttributeError:
            self.to_search = request.GET

        # saving and getting last search
        self.to_search = dict((key, item) for
                              key, item in self.to_search.items())
        new_page = self.to_search.pop('page', None)
        if len(self.to_search):
            request.session['last_search'] = self.to_search
        else:
            self.to_search = request.session.get('last_search',
                                                 self.to_search)
            # get new page value
            page = new_page or self.to_search.get('page', 1)
            self.to_search['page'] = page
            # save search with new page
            if page == new_page:
                request.session['last_search'] = self.to_search
            # set page to use
            self.kwargs['page'] = page

        self.form = SearchForm(self.to_search)
        return super(SearchView, self).get(request)

    def get_queryset(self):
        is_valid = True
        if not self.form.is_valid():
            is_valid = False
            return []
        f = self.form.data
        filter_kwargs = {
            'main_picture__isnull': False,
            'ad__status': Ad.IN_USE
        }

        if f.get('referencia', False):
            filter_kwargs['id'] = f['referencia']
            return Inmueble.full_objects.filter(**filter_kwargs)
        # If is a type of Inmueble it selects the type and filter
        if f.get('tipo_de_inmueble', False):
            AdvancedForm = create_form_str(f['tipo_de_inmueble'])
            self.advanced_form = AdvancedForm(self.to_search,
                                              prefix='advanced')

        # This shouldnet be here, lets find other solution
        # this is a dirty hack
            is_valid = is_valid and self.advanced_form.is_valid()
            if is_valid:
                result = self.advanced_form.queryset().filter(**filter_kwargs)
            else:
                return []
        else:
            result = Inmueble.full_objects.filter(**filter_kwargs).distinct()

        if f.get('divisa', False):
            divisa = Currency.objects.get(id=f['divisa'])
        else:
            divisa = Currency.objects.get(code='MXN')
        if f.get('operacion', False):
            result = result.filter(transaccion=f['operacion'])
        if f.get('codigo_postal', False):
            result = result\
                .filter(colonia__postal_code__number=f['codigo_postal'])
        if f.get('estado', False):
            result = result\
                .filter(colonia__municipality__state__id=f['estado'])
        if f.get('colonia', False):
            result = result.filter(colonia__name__icontains=f['colonia'])
        if f.get('precio_minimo', False):
            quantity = divisa.convert_to_global(f['precio_minimo'])
            result = result.filter(precio_global__gte=quantity)
        if f.get('precio_maximo', False):
            quantity = divisa.convert_to_global(f['precio_maximo'])
            result = result.filter(precio_global__lte=quantity)
        if f.get('superficie_minima', False):
            result = result\
                .filter(_surface__gte=f['superficie_minima'])
        if f.get('superficie_maxima', False):
            result = result\
                .filter(_surface__lte=f['superficie_maxima'])
        return result.distinct()

    def get_context_data(self, **kwargs):
        context = super(SearchView, self).get_context_data(**kwargs)
        context['form'] = self.form
        context['advanced_form'] = getattr(self, 'advanced_form', None)
        return context


create_property_view = CreatePropertyView.as_view()
form_view = FormView.as_view()
property_edit_view = PropertyEditView.as_view()
property_detail_view = PropertyDetailView.as_view()
user_properties_view = UserPropertiesView.as_view()
create_image_view = CreateImageView.as_view()
search_view = SearchView.as_view()
set_favourite_view = SetFavouriteView.as_view()
delete_image_view = DeleteImageView.as_view()
set_main_picture_view = SetMainPictureView.as_view()
custom_search_form_view = CustomSearchFormView.as_view()
delete_property_view = DeletePropertyView.as_view()
