# coding: utf-8
from django.db import models
from django.contrib.auth.models import User
from django.contrib.humanize.templatetags.humanize import intcomma
from django.db.models import BooleanField, CharField, TextField
from django.db.models.query import QuerySet

from sepomex.models import State, Settlement
from currencies.models import Currency
from ads.models import Ad


ORIENTATION_CHOICES = (
    ('N', 'norte'),
    ('S', 'sur'),
    ('E', 'este'),
    ('O', 'oeste'),
)


class SubClassingQuerySet(QuerySet):
    def __getitem__(self, k):
        result = super(SubClassingQuerySet, self).__getitem__(k)
        if isinstance(result, models.Model):
            return result.as_leaf_class()
        else:
            return result

    def __iter__(self):
        for item in super(SubClassingQuerySet, self).__iter__():
            yield item.as_leaf_class()


class InmuebleManager(models.Manager):
    def get_query_set(self):
        return SubClassingQuerySet(self.model)


def choices_range(start, end, by=1):
    return [(i, i) for i in range(start, end + 1, by)]


class AdicionalesImportantesMixin(models.Model):

    EXCELENT = u'E'
    BUENO = u'B'
    REMODELAR = u'R'

    CONSERVACION = (
        (EXCELENT, 'excelent'),
        (BUENO, 'bueno'),
        (REMODELAR, 'remodelar'),
    )

    FORMA_ENTREGA = (
        ('G', 'obra gris'),
        ('B', 'obra blanca'),
        ('T', 'terminada'),
        ('A', 'amueblado'),
    )

    conservacion = CharField(u'conservación', max_length=2,
                             choices=CONSERVACION,
                             default=BUENO)
    estacionamientos = models.SmallIntegerField('No. Estacionamientos',
                                                default=0)

    forma_entrega = models.CharField(u'forma de entrega',
                                     max_length=1, choices=FORMA_ENTREGA)

    class Meta:
        abstract = True


class ExtrasInmuebleMixin(models.Model):

    tiene_alacena = BooleanField('alacena')
    tiene_closet_blancos = BooleanField('closet de blancos')
    tiene_closets = BooleanField('closets')
    tiene_area_lavado = BooleanField('Área de lavado')
    tiene_servicio = BooleanField('Cuarto de Servicio')
    tiene_sala_tele = BooleanField('Sala de tele')
    tiene_estudio = BooleanField('Estudio')
    tiene_biblioteca = BooleanField('biblioteca')

    class Meta:
        abstract = True


class ServiciosCondominioMixin(models.Model):

    tiene_snack_bar = BooleanField('snack bar')
    tiene_salon_fiesta = BooleanField('salón de fiestas')
    tiene_pista_jogging = BooleanField('pista jogging')
    tiene_ludoteca = BooleanField('ludoteca')

    class Meta:
        abstract = True


class TemperaturaMixin(models.Model):
    tiene_calefaccion = BooleanField('calefacción')
    tiene_acondicionado = BooleanField('aire acondicionado')

    class Meta:
        abstract = True


class Imagen(models.Model):
    EDIT_SIZE = "165x85"
    PROFILE_LIST_SIZE = "175x110"
    descripcion = CharField('descripción', max_length=140, blank=True)
    imagen = models.ImageField('imágen', upload_to='properties')


class Inmueble(models.Model):

    VENTA = u'V'
    RENTA = u'R'
    TRASPASO = u'T'

    TIPOS_DE_TRANSACCION = (
        (VENTA, u'Venta'),
        (RENTA, u'Renta'),
        (TRASPASO, u'Traspaso'),
    )

    VISIBLE = 'VIS'
    ESCONDIDA = 'ESC'
    BORRADA = 'BOR'

    ESTADOS = (
        (VISIBLE, 'visible'),
        (ESCONDIDA, 'escondida'),
        (BORRADA, 'borrada'),
    )

    COMISION_CHOICES = (
        (20, 20),
        (30, 30),
        (40, 40),
        (50, 50),
        (60, 60),
    )

    user = models.ManyToManyField(User, editable=False)
    favourite_of = models.ManyToManyField(User, editable=False,
                                          related_name='favourites')
    colonia = models.ForeignKey(Settlement)
    calle_y_num = TextField(u'calle y número', max_length=300,
                            blank=True)
    referencias = TextField(u'referencia ubicación', max_length=300,
                            blank=True)
    precio = models.DecimalField(u'Precio', max_digits=14,
                                 decimal_places=2)
    precio_global = models.DecimalField(max_digits=14,
                                        decimal_places=2, editable=False,
                                        null=True)
    divisa = models.ForeignKey(Currency)
    transaccion = CharField(u'Tipo de transacción',
                            max_length=1, choices=TIPOS_DE_TRANSACCION)
    descripcion = TextField(u'Descripción del inmueble', blank=True)
    observaciones = TextField(u'Observaciones', blank=True)
    pct_pactada = models.FloatField(u'% Comisión Pactada')
    pct_compartir = models.IntegerField(u'% Comisión a Compartir',
                                        choices=COMISION_CHOICES)

    lat = models.FloatField(editable=False, null=True)
    lon = models.FloatField(editable=False, null=True)

    main_picture = models.ForeignKey('Imagen', null=True, blank=True,
                                     editable=False,
                                     related_name='main_pictures')
    imagenes = models.ManyToManyField('Imagen', null=True, blank=True)
    state = models.CharField('estado', max_length=3, default=ESCONDIDA,
                             choices=ESTADOS, editable=False)
    add_date_time = models.DateTimeField('Fecha que se agrego',
                                         auto_now_add=True, editable=False)
    last_update = models.DateTimeField('actualización', auto_now=True,
                                       editable=False)
    type = models.CharField('tipo inmueble', max_length=3, editable=False)

    ad = models.ForeignKey(Ad, blank=True, null=True, verbose_name='anuncio')
    _surface = models.IntegerField(editable=False)

    full_objects = InmuebleManager()
    objects = models.Manager()

    # TODO: Reorganizar esto!!
    _exclude_fields = ('type', 'ad', 'add_date_time', 'user', 'referencias')
    _address_fields = ('estado', 'codigo_postal', 'colonia', 'calle_y_num')
    _general_fields_free = ('precio', 'mantenimiento', 'nombre_desarrollo',
                            'superficie_total', 'superficie_construida',
                            'habitaciones', 'vestidores', 'banos',
                            'medios_banos', 'edad', 'tiene_family_room',
                            'tiene_antecomedor', 'tiene_desnivel',
                            'oficinas_privadas', 'salas_de_juntas',
                            'num_elevadores', 'frente', 'fondo',)
    _general_fields_choices = ('divisa', 'transaccion', 'niveles',
                               'piso', 'niveles_permitidos',
                               'pct_sin_construir', 'areas_comunes',
                               'tiene_mezanine', 'altura', 'tipo_fachada',
                               'tiene_hidraulica_electrica', 'tiene_tapanco',
                               'resistencia_piso', 'anden', 'tiene_oficinas',
                               'tiene_rampa', 'entrada_luz', 'tiene_patio',
                               'tiene_banos', 'tiene_banos_ejecutivos',
                               'tiene_doble_altura', 'tiene_roof_garden',)

    _extras_left = (
        'orientacion', 'tiene_alacena', 'tiene_closet_blancos',
        'tiene_closets', 'tiene_area_lavado', 'tiene_servicio',
        'tiene_sala_tele', 'tiene_estudio', 'tiene_biblioteca',
        'tiene_calefaccion', 'tiene_acondicionado',
        'tiene_panoramica',
    )

    _extras_right = (
        'tiene_bodega', 'tiene_gimnasio', 'tiene_jardin',
        'tiene_alberca', 'tiene_tenis', 'tiene_padel',
        'tiene_planta_luz', 'tiene_terraza', 'tiene_seguridad',
        'tiene_cocina', 'tiene_comedor', 'tiene_recepcion',
        'tiene_sisterna', 'tiene_seguridad',
    )

    _condominio_left = ('tiene_snack_bar', 'tiene_salon_fiesta')
    _condominio_right = ('tiene_pista_jogging', 'tiene_ludoteca')

    _adicionales_left = ('conservacion', 'estacionamientos',
                         'tipo_ubicacion')
    _adicionales_right = ('forma_entrega', 'estacionamientos_visitas')

    _general_fields = _general_fields_free + _general_fields_choices
    _extras_inmueble = _extras_left + _extras_right
    _condominio = _condominio_left + _condominio_right
    _adicionales = _adicionales_left + _adicionales_right

    _all_fields = _general_fields + _extras_inmueble\
        + _condominio + _adicionales

    def get_google_maps_query(self):
        colonia = self.colonia
        municipality = colonia.municipality
        return u"{municipality}, {CP}, {state}, "\
            u"México".format(CP=self.codigo_postal,
                              municipality=municipality.name,
                              state=municipality.state.name)

    def delete(self, *args, **kwargs):
        #self.imagenes.all().delete()
        if self.ad:
            self.ad.set_ready()
        super(Inmueble, self).delete(*args, **kwargs)

    @property
    def estado(self):
        return State.objects.distinct()\
            .get(municipality=self.municipio)

    @property
    def codigo_postal(self):
        return str(self.colonia.postal_code.number).rjust(5, '0')

    @property
    def owner(self):
        try:
            return self.__owner
        except AttributeError:
            pass
        self.__owner = self.user.all()[0]
        return self.__owner

    @property
    def municipio(self):
        return self.colonia.municipality

    def update_global_price(self, save=True):
        """
        Update global price to USD
        This method is not recomended
        to update the whole database
        """
        self.precio_global = self.divisa.convert_to_global(self.precio)
        if save:
            self.save()

    def save(self, *args, **kwargs):
        if not self.type:
            self.type = self._type

        self._set_surface()
        self.update_global_price(save=False)
        return super(Inmueble, self).save(*args, **kwargs)

    def _set_surface(self):
        if type(self).__name__ == 'Inmueble':
            return
        try:
            surface = self.superficie_construida
        except AttributeError:
            surface = self.superficie_total
        self._surface = surface

    def as_leaf_class(self):
        return TYPE_CLASS[self.type].objects.get(id=self.id)

    def is_favourite_of(self, user):
        if user.favourites.filter(id=self.id).exists():
            return True
        return False

    @property
    def leaf(self):
        return self.as_leaf_class()

    @property
    def tipo_inmueble(self):
        return TIPOS_DE_INMUEBLE[self.type]

    @property
    def precio_completo(self):
        divisa = self.divisa
        precio = intcomma(self.precio)
        return u' '.join((divisa.prefix, precio, divisa.sufix,
                          divisa.code))

    @property
    def ubicacion(self):
        colonia = self.colonia
        return u', '.join((self.codigo_postal, colonia.name))

    @models.permalink
    def get_absolute_url(self):
        return ('property-detail', [self.id])

    def has_extra_fields(self):
        """ Eval if """
        if len(list(self.get_extra_fields())):
            return True
        return False

    def get_condominio_fields(self):
        fields = self._get_fields(self._condominio)
        return fields

    def get_extra_fields(self):
        return self._get_fields(self._extras_inmueble)

    def get_general_fields(self):
        return self._get_fields(self._general_fields)

    def get_adicionales_fields(self):
        return self._get_fields(self._adicionales)

    def _get_fields(self, fields):
        own_fields = set(self._meta.get_all_field_names())
        own_fields.intersection_update(set(fields))
        names = sorted(own_fields)
        gf = self._meta.get_field_by_name
        fields = [gf(name)[0] for name in names]
        clean_fields = []

        # remove boolean False fields
        for field in fields:
            value = getattr(self, field.name)
            # if no is a boolean value it just add the field
            if not isinstance(value, bool):
                clean_fields.append(field)
            # if is a boolean field must be true to be added
            elif value:
                clean_fields.append(field)
        return clean_fields

    def has_newer_similar(self):
        return self.flagged_similar_set.exists()

    def has_older_similar(self):
        return 1


class SimilarFlagged(models.Model):
    inmueble = models.ForeignKey(Inmueble, unique=True,
                                 related_name='similar_flaged')
    similars = models.ManyToManyField(Inmueble,
                                      related_name='similar_flaged_newer')


class Terreno(Inmueble):
    HABITACIONAL = 'THB'
    COMERCIAL = 'TCM'
    INDUSTRIAL = 'TID'
    CONDOMINIO = 'TCH'

    TIPOS = (
        (HABITACIONAL, 'habitacional'),
        (COMERCIAL, 'comercial'),
        (CONDOMINIO, 'condominio habitacional'),
        (INDUSTRIAL, 'industrial'),
    )

    tipo = models.CharField('tipo', max_length=3, choices=TIPOS)
    superficie_total = models.IntegerField(u'Superficie (m²)')
    tiene_desnivel = BooleanField('desnivel')
    frente = models.IntegerField(u'frente en m²', default=0)
    fondo = models.IntegerField(u'fondo en m²', default=0)
    niveles_permitidos = models.IntegerField('niveles/plantas permitidos',
                                             default=0)
    # validar 0 <= x <= 100
    pct_sin_construir = models.IntegerField('Area sin construir (%)',
                                            default=100)
    orientacion = models.CharField('Orientación', max_length=1,
                                   choices=ORIENTATION_CHOICES)

    def save(self, *args, **kwargs):
        if not self.type:
            self.type = self.tipo
        return super(Terreno, self).save(*args, **kwargs)


class CasaComercialOficina(Inmueble,
                           TemperaturaMixin,
                           AdicionalesImportantesMixin):
    _type = 'CCO'
    NIVELES = choices_range(1, 6)
    superficie_construida = models.IntegerField(u'Construcción (m²)')
    superficie_total = models.IntegerField(u'Superficie Terreno (m²)')
    banos = models.IntegerField(u'baños', null=True, default=0)
    niveles = models.IntegerField('niveles/plantas', choices=NIVELES)
    edad = models.IntegerField(u'edad (años)', default=0)
    tiene_desnivel = BooleanField('desnivel')
    oficinas_privadas = models.IntegerField(u'oficinas privadas', default=0)
    salas_de_juntas = models.SmallIntegerField('salas de juntas', default=0)
    tiene_bodega = BooleanField('bodega')
    tiene_jardin = BooleanField('jardín')
    tiene_terraza = BooleanField('terraza privada')
    tiene_cocina = BooleanField('cocina')


class CasaSola(Inmueble,
               ExtrasInmuebleMixin,
               TemperaturaMixin,
               AdicionalesImportantesMixin):
    _type = 'CSL'
    NIVELES = choices_range(1, 4)
    superficie_construida = models.IntegerField(u'Construcción (m²)')
    superficie_total = models.IntegerField(u'Superficie Terreno (m²)')
    habitaciones = models.IntegerField('habitaciones', null=True, default=0)
    vestidores = models.IntegerField('vestidores', null=True, default=0)
    banos = models.IntegerField(u'baños', null=True, default=0)
    medios_banos = models.IntegerField(u'Medios baños', null=True, default=0)
    niveles = models.IntegerField('niveles/plantas', choices=NIVELES)
    edad = models.IntegerField(u'edad (años)')

    tiene_family_room = BooleanField('family room')
    tiene_antecomedor = BooleanField('antecomedor')
    tiene_desnivel = BooleanField('desnivel')
    tiene_bodega = BooleanField('bodega')
    tiene_gimnasio = BooleanField('gimnasio')
    tiene_jardin = BooleanField('jardín')
    tiene_alberca = BooleanField('alberca')
    tiene_tenis = BooleanField('cancha de tenis')
    tiene_padel = BooleanField('cancha de padel')
    tiene_planta_luz = BooleanField('planta de luz')
    orientacion = models.CharField('Orientación', max_length=1,
                                   choices=ORIENTATION_CHOICES)
    tiene_comedor = BooleanField('comedor')
    tiene_recepcion = BooleanField('recepcion')


class CasaEnPrivada(Inmueble,
                    ExtrasInmuebleMixin,
                    TemperaturaMixin,
                    AdicionalesImportantesMixin):
    _type = 'CPV'
    NIVELES = choices_range(1, 6)
    mantenimiento = models.DecimalField(u'Mantenimiento', max_digits=14,
                                        decimal_places=2, null=True)
    superficie_construida = models.IntegerField(u'Construcción (m²)')
    superficie_total = models.IntegerField(u'Superficie Terreno (m²)')
    habitaciones = models.IntegerField('habitaciones', null=True, default=0)
    vestidores = models.IntegerField('vestidores', null=True, default=0)
    banos = models.IntegerField(u'baños', null=True, default=0)
    medios_banos = models.IntegerField(u'Medios baños', null=True, default=0)
    niveles = models.IntegerField('niveles/plantas', choices=NIVELES)
    edad = models.IntegerField(u'edad (años)')

    tiene_family_room = BooleanField('family room')
    tiene_antecomedor = BooleanField('antecomedor')
    tiene_desnivel = BooleanField('desnivel')
    tiene_bodega = BooleanField('bodega')
    tiene_gimnasio = BooleanField('gimnasio')
    tiene_jardin = BooleanField('jardín')
    tiene_alberca = BooleanField('alberca')
    tiene_tenis = BooleanField('cancha de tenis')
    tiene_padel = BooleanField('cancha de padel')
    tiene_planta_luz = BooleanField('planta de luz')
    orientacion = models.CharField('Orientación', max_length=1,
                                   choices=ORIENTATION_CHOICES)
    tiene_seguridad = BooleanField('seguridad')


class CasaEnCondominio(Inmueble,
                       ExtrasInmuebleMixin,
                       TemperaturaMixin,
                       ServiciosCondominioMixin,
                       AdicionalesImportantesMixin):
    _type = 'CCD'

    NIVELES = choices_range(1, 4)
    nombre_desarrollo = CharField('nombre del desarrollo', max_length=140)
    mantenimiento = models.DecimalField(u'Mantenimiento', max_digits=14,
                                        decimal_places=2, null=True)
    superficie_construida = models.IntegerField(u'Construcción (m²)')
    superficie_total = models.IntegerField(u'Superficie Terreno (m²)')
    habitaciones = models.IntegerField('habitaciones', null=True, default=0)
    vestidores = models.IntegerField('vestidores', null=True, default=0)
    banos = models.IntegerField(u'baños', null=True, default=0)
    medios_banos = models.IntegerField(u'Medios baños', null=True, default=0)
    niveles = models.IntegerField('niveles/plantas', choices=NIVELES)
    edad = models.IntegerField(u'edad (años)')

    tiene_family_room = BooleanField('family room')
    tiene_antecomedor = BooleanField('antecomedor')
    tiene_desnivel = BooleanField('desnivel')
    estacionamientos_visitas = models.SmallIntegerField('No. Estacionamientos'
                                                        ' visitas', default=0)
    tiene_panoramica = BooleanField('Vistas panorámicas')
    tiene_bodega = BooleanField('bodega')
    tiene_gimnasio = BooleanField('gimnasio')
    tiene_jardin = BooleanField('jardín')
    tiene_alberca = BooleanField('alberca')
    tiene_tenis = BooleanField('cancha de tenis')
    tiene_padel = BooleanField('cancha de padel')
    tiene_planta_luz = BooleanField('planta de luz')
    tiene_terraza = BooleanField('terraza privada')
    orientacion = models.CharField('Orientación', max_length=1,
                                   choices=ORIENTATION_CHOICES)
    tiene_seguridad = BooleanField('seguridad')


class Departamento(AdicionalesImportantesMixin,
                   ExtrasInmuebleMixin,
                   TemperaturaMixin,
                   ServiciosCondominioMixin,
                   Inmueble):
    _type = 'DEP'

    NIVELES = choices_range(1, 2)

    nombre_desarrollo = CharField('nombre del desarrollo', max_length=140)
    mantenimiento = models.DecimalField(u'Mantenimiento', max_digits=14,
                                        decimal_places=2, null=True)
    superficie_construida = models.IntegerField(u'Construcción (m²)')
    habitaciones = models.IntegerField('habitaciones', null=True, default=0)
    vestidores = models.IntegerField('vestidores', null=True, default=0)
    banos = models.IntegerField(u'baños', null=True, default=0)
    medios_banos = models.IntegerField(u'Medios baños', null=True, default=0)

    # TODO: aceptar PH, PB, y numeros
    piso = models.CharField(u'¿En qué piso se encuentra?', max_length=4,
                            blank=True)
    edad = models.IntegerField(u'edad (años)')
    orientacion = models.CharField('Orientación', max_length=1,
                                   choices=ORIENTATION_CHOICES)
    niveles = models.IntegerField('niveles/plantas', choices=NIVELES)
    estacionamientos_visitas = models.SmallIntegerField('No. Estacionamientos'
                                                        ' visitas', default=0)

    tiene_family_room = BooleanField('family room')
    tiene_antecomedor = BooleanField('antecomedor')
    tiene_doble_altura = BooleanField('doble altura')
    tiene_roof_garden = BooleanField('roof garden')
    tiene_panoramica = BooleanField('Vistas panorámicas')
    tiene_bodega = BooleanField('bodega')
    tiene_gimnasio = BooleanField('gimnasio')
    tiene_jardin = BooleanField('jardín')
    tiene_alberca = BooleanField('alberca')
    tiene_tenis = BooleanField('cancha de tenis')
    tiene_padel = BooleanField('cancha de padel')
    tiene_planta_luz = BooleanField('planta de luz')
    tiene_terraza = BooleanField('terraza privada')
    tiene_seguridad = BooleanField('seguridad')


class Loft(AdicionalesImportantesMixin,
           ExtrasInmuebleMixin,
           TemperaturaMixin,
           ServiciosCondominioMixin,
           Inmueble):
    _type = 'LFT'
    nombre_desarrollo = CharField('nombre del desarrollo', max_length=140)
    mantenimiento = models.DecimalField(u'Mantenimiento', max_digits=14,
                                        decimal_places=2, null=True)
    superficie_construida = models.IntegerField(u'Construcción (m²)')
    habitaciones = models.IntegerField('habitaciones', null=True, default=0)
    vestidores = models.IntegerField('vestidores', null=True, default=0)
    banos = models.IntegerField(u'baños', null=True, default=0)
    medios_banos = models.IntegerField(u'Medios baños', null=True, default=0)
    piso = models.CharField(u'¿En qué piso se encuentra?', max_length=4,
                            blank=True)
    edad = models.IntegerField(u'edad (años)')

    tiene_family_room = BooleanField('family room')
    tiene_antecomedor = BooleanField('antecomedor')
    tiene_doble_altura = BooleanField('doble altura')
    estacionamientos_visitas = models.SmallIntegerField('No. Estacionamientos'
                                                        ' visitas', default=0)
    tiene_bodega = BooleanField('bodega')
    tiene_gimnasio = BooleanField('gimnasio')
    tiene_jardin = BooleanField('jardín')
    tiene_alberca = BooleanField('alberca')
    tiene_tenis = BooleanField('cancha de tenis')
    tiene_padel = BooleanField('cancha de padel')
    tiene_planta_luz = BooleanField('planta de luz')
    tiene_terraza = BooleanField('terraza privada')
    tiene_seguridad = BooleanField('seguridad')


# Oficina en Edificio
class Oficina(Inmueble,
              AdicionalesImportantesMixin):
    _type = 'OFN'

    FLOORS = [('PB', 'PB')] + choices_range(1, 50)
    FLOORS = [(str(x), str(y)) for x, y in FLOORS]

    mantenimiento = models.DecimalField(u'Mantenimiento', max_digits=14,
                                        decimal_places=2, null=True)
    superficie_construida = models.IntegerField(u'Construcción (m²)')
    superficie_total = models.IntegerField(u'Superficie Terreno (m²)')

    banos = models.IntegerField(u'baños', null=True, default=0)
    piso = models.CharField(u'¿En qué piso se encuentra?', max_length=4,
                            blank=True, choices=FLOORS)
    edad = models.IntegerField(u'edad (años)')
    oficinas_privadas = models.IntegerField(u'oficinas privadas', default=0)
    salas_de_juntas = models.SmallIntegerField('salas de juntas', default=0)
    estacionamientos_visitas = models.SmallIntegerField('No. Estacionamientos'
                                                        ' visitas', default=0)
    tiene_bodega = BooleanField('bodega')
    tiene_terraza = BooleanField('terraza privada')
    tiene_cocina = BooleanField('cocina')
    tiene_comedor = BooleanField('comedor')
    tiene_recepcion = BooleanField('recepcion')


class EdificioOficinas(Inmueble,
                       AdicionalesImportantesMixin):
    _type = 'EOF'

    # estacionamientos = 0 - 200
    # niveles = 0 - 80
    NIVELES = choices_range(1, 50)

    superficie_construida = models.IntegerField(u'Construcción (m²)')
    superficie_total = models.IntegerField(u'Superficie Terreno (m²)')
    banos = models.IntegerField(u'baños', null=True, default=0)
    niveles = models.IntegerField('niveles/plantas', choices=NIVELES)
    edad = models.IntegerField(u'edad (años)')
    oficinas_privadas = models.IntegerField(u'oficinas privadas', default=0)
    salas_de_juntas = models.SmallIntegerField('salas de juntas', default=0)
    num_elevadores = models.SmallIntegerField(u'número de elevadores',
                                              default=0)
    areas_comunes = models.IntegerField(u'areas comunes (m²)')
    estacionamientos_visitas = models.SmallIntegerField('No. Estacionamientos'
                                                        ' visitas', default=0)

    tiene_calefaccion = BooleanField('calefacción central')
    tiene_acondicionado = BooleanField('aire acondicionado central')
    tiene_terraza = BooleanField('terraza privada')
    tiene_bodega = BooleanField('bodegas')
    tiene_planta_luz = BooleanField('planta de luz')
    tiene_terraza = BooleanField('terraza privada')
    tiene_cocina = BooleanField('cocina')
    tiene_sisterna = BooleanField('sisterna')
    tiene_seguridad = BooleanField('caseta vigilancia')


class LocalComercial(Inmueble):
    _type = 'LCM'

    TIPOS_FACHADAS = (
        ('COR', 'cortina'),
        ('CRI', 'cristal'),
    )

    TIPO_UBICACION = (
        ('PLZ', 'plaza comercial'),
        ('EDF', 'edificio'),
        ('SOL', 'stand alone'),
    )

    FORMA_ENTREGA = (
        ('G', 'obra gris'),
        ('B', 'obra blanca'),
        ('T', 'terminada'),
        ('A', 'amueblado'),
    )

    ALTURAS = choices_range(2, 6)
    TAM_FRENTES = choices_range(2, 50)
    NIVELES = choices_range(1, 5)

    mantenimiento = models.DecimalField(u'Mantenimiento', max_digits=14,
                                        decimal_places=2, null=True)
    superficie_construida = models.IntegerField(u'Construcción (m²)')
    tiene_banos = models.BooleanField(u'baño')
    niveles = models.IntegerField('niveles/plantas', choices=NIVELES)
    frente = models.SmallIntegerField(u'frente en m²', choices=TAM_FRENTES)
    fondo = models.IntegerField(u'fondo en m²', default=0)
    tiene_mezanine = BooleanField('mezanine')
    altura = models.SmallIntegerField('altura (m)', choices=ALTURAS)
    tipo_fachada = models.CharField('fachada', max_length=3,
                                    choices=TIPOS_FACHADAS)
    tiene_hidraulica_electrica = models.BooleanField(u'Instalación Hidraulica'
                                                     u' y Eléctrica')

    tipo_ubicacion = models.CharField(u'típo ubicación', max_length=3,
                                      choices=TIPO_UBICACION)
    tiene_tapanco = BooleanField('tapanco')
    tiene_bodega = BooleanField('bodega')

    forma_entrega = models.CharField(u'forma de entrega',
                                     max_length=1, choices=FORMA_ENTREGA)


class Bodega(Inmueble):
    _type = 'BDG'

    ALTURAS = choices_range(2, 6)
    RESISTENCIAS_PISO = choices_range(3, 30)
    TAM_FRENTES = choices_range(2, 50)

    BIFASICA = 'B'
    TRIFASICA = 'T'

    ENTRADAS_LUZ = (
        (BIFASICA, 'bifásica'),
        (TRIFASICA, 'trifásica'),
    )

    superficie_construida = models.IntegerField(u'Construcción (m²)')
    frente = models.SmallIntegerField(u'frente en m²', choices=TAM_FRENTES)
    fondo = models.IntegerField(u'fondo en m²', default=0)
    altura = models.SmallIntegerField('altura (m)', choices=ALTURAS)
    resistencia_piso = models.FloatField('resistencia del piso m',
                                         choices=RESISTENCIAS_PISO)
    anden = models.IntegerField(u'anden (Res x m²)', default=0)
    tiene_oficinas = BooleanField('oficinas')
    tiene_rampa = BooleanField('rampa de carga y descarga')
    entrada_luz = models.CharField('tipo de entrada de luz', max_length=2,
                                   choices=ENTRADAS_LUZ)
    tiene_patio = BooleanField(u'patio de maniobras')
    tiene_banos = models.BooleanField(u'baños obreros')
    tiene_banos_ejecutivos = models.BooleanField(u'baños ejecutivos')
    tiene_seguridad = BooleanField('caseta vigilancia')


class NaveIndustrial(Inmueble):
    _type = 'NIN'

    ALTURAS = choices_range(2, 6)
    TAM_FRENTES = choices_range(2, 50)
    RESISTENCIAS_PISO = choices_range(3, 30)

    BIFASICA = 'B'
    TRIFASICA = 'T'

    ENTRADAS_LUZ = (
        (BIFASICA, 'bifásica'),
        (TRIFASICA, 'trifásica'),
    )

    superficie_construida = models.IntegerField(u'Construcción (m²)')
    frente = models.SmallIntegerField(u'frente en m²', choices=TAM_FRENTES)
    fondo = models.IntegerField(u'fondo en m²', default=0)
    altura = models.SmallIntegerField('altura (m)', choices=ALTURAS)
    resistencia_piso = models.FloatField('resistencia del piso m',
                                         choices=RESISTENCIAS_PISO)
    anden = models.IntegerField(u'anden (Res x m²)', default=0)
    tiene_oficinas = BooleanField('oficinas')
    tiene_rampa = BooleanField('rampa de carga y descarga')
    entrada_luz = models.CharField('tipo de entrada de luz', max_length=2,
                                   choices=ENTRADAS_LUZ)
    tiene_patio = BooleanField(u'patio de maniobras')
    tiene_banos = models.BooleanField(u'baños obreros')
    tiene_banos_ejecutivos = models.BooleanField(u'baños ejecutivos')


TYPE_CLASS = {
    u'DEP': Departamento,
    u'LFT': Loft,
    u'CSL': CasaSola,
    u'CCD': CasaEnCondominio,
    u'CPV': CasaEnPrivada,
    u'CCO': CasaComercialOficina,
    u'THB': Terreno,
    u'TCM': Terreno,
    u'TCH': Terreno,
    u'TID': Terreno,
    u'OFN': Oficina,
    u'EOF': EdificioOficinas,
    u'LCM': LocalComercial,
    u'BDG': Bodega,
    u'NIN': NaveIndustrial,
}

TIPOS_DE_INMUEBLE = {
    u'DEP': u'Departamento',
    u'LFT': u'Loft',
    u'CSL': u'Casa Sola',
    u'CCD': u'Casa en Condominio',
    u'CPV': u'Casa en Privada',
    u'CCO': u'Oficina en Casa',
    u'THB': u'Terreno Habitacional',
    u'TCM': u'Terreno Comercial',
    u'TCH': u'Terreno Condominio Habitacional',
    u'TID': u'Terreno Industrial',
    u'OFN': u'Oficina',
    u'EOF': u'Edificio de Oficinas',
    u'LCM': u'Local comercial',
    u'BDG': u'Bodega',
    u'NIN': u'Nave Industrial',
}
