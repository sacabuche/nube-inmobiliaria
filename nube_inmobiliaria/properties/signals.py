from django.db.models.signals import post_init, post_save
from properties.models import Inmueble, TYPE_CLASS


inmueble_models = TYPE_CLASS.values() + [Inmueble]


def cache_last_ad(sender, instance, *args, **kwargs):
    instance.__original_ad = instance.ad


def update_ad(sender, instance, created, **kwargs):
    original_ad = instance.__original_ad
    new_ad = instance.ad
    if original_ad != new_ad:
        # may be is null
        try:
            original_ad.set_ready()
        except AttributeError:
            pass

        # may be is null
        try:
            new_ad.set_in_use(instance)
        except AttributeError:
            pass


def attach_signal(signal, function, models, *args, **kwargs):
    for model in models:
        signal.connect(function, sender=model, *args, **kwargs)


def compare_inmueble(sender, instance, *args, **kwargs):
    simi = Inmueble.objects.filter(colonia=instance.colonia,
                                   type=instance.type)
    if simi.count():
        pass


attach_signal(post_init, cache_last_ad, inmueble_models)
attach_signal(post_save, update_ad, inmueble_models)
attach_signal(post_save, compare_inmueble, inmueble_models)
