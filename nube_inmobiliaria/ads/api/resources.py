from tastypie.resources import ModelResource

from ads.models import BaseAd


class BaseAdResource(ModelResource):
    class Meta:
        queryset = BaseAd.objects.all()
        resource_name = 'base_ad'
        limit = 10000
