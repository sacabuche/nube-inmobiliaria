from django.conf.urls.defaults import patterns, include

from tastypie.api import Api
from ads.api.resources import BaseAdResource

v1_api = Api(api_name='v1')
v1_api.register(BaseAdResource())

urlpatterns = patterns('',
    (r'^api/', include(v1_api.urls)),
)
