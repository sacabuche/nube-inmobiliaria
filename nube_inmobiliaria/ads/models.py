# coding: utf-8
from datetime import timedelta
from decimal import Decimal

from django.utils import timezone
from django.db import models, transaction
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType


DAYS_IN_YEAR = 365
DAYS_IN_MONTH = 30

MINUTE_SECONDS = 60
HOUR_SECONDS = 60 * MINUTE_SECONDS
DAY_SECONDS = 24 * HOUR_SECONDS
WEEK_SECONDS = 7 * DAY_SECONDS
MONTH_SECONDS = DAYS_IN_MONTH * DAY_SECONDS
TAX = 0.16

COMPUTER_RANGE = (
    #    1,
    #    MINUTE_SECONDS,
    #    HOUR_SECONDS,
    DAY_SECONDS,
    WEEK_SECONDS,
    MONTH_SECONDS
)

HUMAN_RANGE = (
    #    ('segundo', 'segundos'),
    #    ('minuto', 'minutos'),
    #    ('hora', 'horas'),
    (u'día', 'dias'),
    ('semana', 'semanas'),
    ('mes', 'meses'),
)


def humanize_time(value, stop=False):
    if value == 0:
        return '0'
    for i, base in enumerate(COMPUTER_RANGE):
        if base > value:
            if i > 0:
                i -= 1
            break
    computer_base = COMPUTER_RANGE[i]
    human_text = HUMAN_RANGE[i]
    human_number = int(value / computer_base)

    if stop and human_number == 0:
        return ''

    if stop or i <= 0:
        second_number = ''
    else:
        second_number = humanize_time(value % computer_base, stop=True)

    if human_number == 1:
        human_text = human_text[0]
    else:
        human_text = human_text[1]

    return '%s %s %s' % (human_number, human_text, second_number)


class BaseMembership(models.Model):
    price = models.DecimalField('precio', max_digits=10, decimal_places=2)
    name = models.CharField('nombre', max_length=150, blank=True, unique=True)
    description = models.TextField(u'descripción', blank=True)
    include = models.ForeignKey('BaseAd', verbose_name='incluye', null=True,
                                blank=True)

    def __unicode__(self):
        return u'Membresía %s %s' % (self.name, self.price)

    def gen(self, user, days=None):
        if days:
            Membership.objects.create(user=user, days=days)
        else:
            Membership.objects.create(user=user)

        if self.include:
            self.include.gen_ads(user)


class Membership(models.Model):
    valid_until = models.DateTimeField(blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User)
    days = models.IntegerField('dias', default=DAYS_IN_YEAR)

    class Meta:
        permissions = (('has_free_membership', "Has free membership"),)

    def __unicode__(self):
        return 'valid until: %s' % self.valid_until

    def save(self, *args, **kwargs):
        if not self.id:
            self.valid_until = timezone.now() + timedelta(days=self.days)
        return super(Membership, self).save(*args, **kwargs)


class BaseAd(models.Model):

    number_of_ads = models.IntegerField('numero de avisos')
    months = models.IntegerField('meses')
    price = models.DecimalField('precio', max_digits=10, decimal_places=2)

    class Meta:
        ordering = ('number_of_ads', 'months', 'price')
        permissions = (("has_free_ads", "Has free ads"),)

    def __unicode__(self):

        txt = u'anuncios:{no_ads}, meses:{months}, precio:{price} + IVA'
        return txt.format(no_ads=self.number_of_ads,
                          months=self.months,
                          price=self.price)

    def gen_ads(self, user):
        for ad in range(self.number_of_ads):
            self.gen_ad(user)

    def gen_ad(self, user):
        return Ad.objects.create(months=self.months,
                                 base_ad=self,
                                 user=user)

    @property
    def tax(self):
        return self.price * Decimal(TAX)

    @property
    def total(self):
        return self.tax + self.price


class Ad(models.Model):

    READY = 0
    IN_USE = 1
    CONSUMED = 2

    STATUS = (
        (READY, 'listo'),
        (IN_USE, 'en uso'),
        (CONSUMED, 'consumido'),
    )

    months = models.IntegerField()
    base_ad = models.ForeignKey(BaseAd)
    time_used = models.FloatField(default=0)
    user = models.ForeignKey(User)
    status = models.SmallIntegerField(default=READY, choices=STATUS)
    valid_until = models.DateTimeField(blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)
    using_since = models.DateTimeField(null=True, blank=True)
    views = models.IntegerField(default=0)
    _content_type = models.ForeignKey(ContentType, editable=False, null=True)
    _content_id = models.IntegerField(null=True, blank=True)

    def __unicode__(self):
        return 'meses:%s, %s' % (self.months,
                                 self.human_time_used)

    @property
    def human_time_used(self):
        if self.status == self.READY:
            return 'tiempo usado: %s' % humanize_time(self.time_used)
        elif self.status == self.IN_USE:
            diff = self.valid_until - timezone.now()
            return u'tiempo restante: %s' % humanize_time(diff.total_seconds())
        else:
            return 'todo el tiempo usado'

    def set_in_use(self, obj):
        if self.status != self.READY:
            return
        with transaction.commit_on_success():
            self.status = self.IN_USE
            self.content = obj
            self.using_since = timezone.now()
            # this must be the last to execute
            self._set_valid_until()
            if self.is_valid:
                self.save()
            else:
                self.set_consumed()

    @property
    def is_valid(self):
        if self.status == self.CONSUMED:
            return False
        elif self.status == self.READY:
            return True

        if self.valid_until < timezone.now():
            self.set_consumed()
            return False
        return True

    @property
    def content(self):
        if not self._content_id:
            return None
        model = self._content_type.model_class()
        return model.objects.get(id=self._content_id)

    @content.setter
    def content(self, obj):
        if obj is None:
            self._content_id = None
            self._content_type = None
            return
        self._content_type = ContentType.objects.get_for_model(obj.__class__)
        self._content_id = obj.id

    def _set_valid_until(self):
        now = timezone.now()
        # set all time used
        time_used = timedelta(seconds=self.time_used)
        time_used = time_used + self.using_since - now
        # get time left
        time_to_use = timedelta(days=self.months * DAYS_IN_MONTH)
        time_left = time_to_use - time_used
        self.valid_until = time_left + now

    def set_consumed(self):
        self.status = self.CONSUMED
        self.using_since = None
        self.save()

    def viewed(self):
        self.views += 1
        self.save()

    def set_ready(self):
        if self.status != self.IN_USE:
            return
        if not self.is_valid:
            self.set_consumed()
        time_used = timezone.now() - self.using_since
        self.time_used += time_used.total_seconds()
        self.status = self.READY
        self.content = None
        self.views = 0
        self.save()
