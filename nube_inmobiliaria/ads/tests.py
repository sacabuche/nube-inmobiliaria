from time import sleep
from datetime import timedelta
from django.utils import unittest, timezone
from django.contrib.auth.models import User

from ads.models import BaseAd, DAYS_IN_MONTH


class BaseAdTestCase(unittest.TestCase):
    def setUp(self):
        self.NUM_ADS = 4
        self.MONTHS = 3
        self.base_ad = BaseAd.objects.create(months=self.MONTHS,
                                             number_of_ads=self.NUM_ADS,
                                             price=500)
        self.user, _ = User.objects.get_or_create(username='juanito',
                                                  email='x@x.com')

    def test_num_ads_generated(self):
        self.base_ad.gen_ads(self.user)
        num_ads = self.base_ad.ad_set.all().count()
        self.assertEqual(self.NUM_ADS, num_ads)

    def test_ad_is_valid(self):
        ad = self.base_ad.gen_ad(self.user)
        self.assertTrue(ad.is_valid)
        ad.time_used = ad.months * DAYS_IN_MONTH * 24 * 60 * 60 - 2
        ad.set_in_use(self.user)
        self.assertTrue(ad.is_valid)
        sleep(2)
        self.assertFalse(ad.is_valid)
        self.assertEquals(ad.status, ad.CONSUMED)

    def test_in_use(self):
        ad = self.base_ad.gen_ad(self.user)
        ad.set_in_use(self.user)  # we just imagin this is our ad Model
        self.assertEqual(self.user, ad.content)
        self.assertTrue(ad.is_valid)
        self.assertEqual(ad.status, ad.IN_USE)

    def test_valid_until(self):
        ad = self.base_ad.gen_ad(self.user)
        now = timezone.now()
        ad.set_in_use(self.user)  # imagin this is our ad Model
        second_now = timezone.now()

        valid_time = ad.valid_until - now
        second_valid_time = ad.valid_until - second_now
        teoric_time = timedelta(days=DAYS_IN_MONTH * ad.months)
        self.assertLess(second_valid_time, teoric_time)
        self.assertGreater(valid_time, teoric_time)

    def test_used_time(self):
        ad = self.base_ad.gen_ad(self.user)
        ad.set_in_use(self.user)
        seconds = 1
        sleep(seconds)
        ad.set_ready()
        self.assertAlmostEqual(ad.time_used, seconds, places=1)
        ad.set_in_use(self.user)
        sleep(seconds)
        ad.set_ready()
        self.assertAlmostEqual(ad.time_used, seconds * 2, places=1)
