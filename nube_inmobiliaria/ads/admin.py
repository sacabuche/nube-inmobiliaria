from django.contrib import admin
from models import BaseAd, Ad, BaseMembership, Membership


admin.site.register(BaseAd)
admin.site.register(Ad)
admin.site.register(BaseMembership)
admin.site.register(Membership)
