from django.core.management.base import NoArgsCommand

from currencies.models import Currency


class Command(NoArgsCommand):
    help = 'Reload All currencies'

    def handle_noargs(self, **options):
        for currency in Currency.objects.all():
            currency.update()
