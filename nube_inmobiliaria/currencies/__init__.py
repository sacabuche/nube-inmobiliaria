from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from django.utils.importlib import import_module
from . import defaults


class RemoteError(Exception):
    pass


BASE_CURRENCY = getattr(settings, 'BASE_CURRENCY', defaults.BASE_CURRENCY)
CURRENCY_BACKEND = getattr(settings, 'CURRENCY_BACKEND', defaults.CURRENCY_BACKEND)


try:
    backend = import_module(CURRENCY_BACKEND)
except  ImportError, e:
    raise ImproperlyConfigured("Error importing %s" % CURRENCY_BACKEND)

try:
    get_new_rate = backend.get_new_rate
except  AttributeError, e:
    raise ImproperlyConfigured("Error importing get_new_rate")
