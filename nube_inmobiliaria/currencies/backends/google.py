import re
from decimal import Decimal, getcontext
import requests

from currencies import RemoteError, BASE_CURRENCY


getcontext().prec = 10
quantity_re = re.compile(r'"(?P<rate>\d*\.?\d+)')
URL = 'http://www.google.com/ig/calculator?q={quantity}{to}=?{base}'


def get_new_rate(to, base=BASE_CURRENCY):
    url = URL.format(quantity=1, base=base, to=to)
    result = requests.get(url)
    if result.status_code != 200:
        raise RemoteError(result.status_code, result.text)
    rate = quantity_re.findall(result.text)[1]
    return Decimal(rate)
