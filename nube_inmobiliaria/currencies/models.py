from decimal import Decimal
from django.db import models
from currencies import get_new_rate


class Currency(models.Model):
    name = models.CharField(u'nombre', max_length=50)
    code = models.CharField(u'codigo ISO 4217', max_length=5, unique=True)
    sufix = models.CharField(u'sufijo', max_length=5, blank=True)
    prefix = models.CharField(u'prefijo', max_length=5, blank=True)
    rate = models.DecimalField(u'tase de cambio', max_digits=14,
                               decimal_places=10, null=True)

    def __unicode__(self):
        return self.code

    def convert_to_global(self, quantity):
        if not isinstance(quantity, Decimal):
            quantity = Decimal(quantity)
        return quantity * self.rate

    def update(self):
        self.rate = get_new_rate(self.code)
        self.save()
