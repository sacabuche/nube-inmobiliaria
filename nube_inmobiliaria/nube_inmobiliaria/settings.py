# Django settings for nube_inmobiliaria project.
import os

PROJECT_DIR = os.path.dirname(__file__)
BASE_DIR = os.path.realpath(PROJECT_DIR)


DEBUG = True
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    ('GTFS', 'drobles@gtsf.com.mx'),
)


EMAIL_HOST = 'smtp.webfaction.com'
EMAIL_HOST_USER = 'nube_servidor'
EMAIL_HOST_PASSWORD = 'xhgc43$'
DEFAULT_FROM_EMAIL = 'Nube Inmobiliaria <servidor@nubeinmobiliaria.com.mx>'
SERVER_EMAIL = DEFAULT_FROM_EMAIL
EMAIL_PORT = 25


MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',  # Add 'postgresql_psycopg2'
        'NAME': os.path.join(BASE_DIR, 'data.sqlite'),
        'USER': '',                      # Not used with sqlite3.
        'PASSWORD': '',                  # Not used with sqlite3.
        'HOST': '',
        'PORT': '',
    }
}

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'America/Mexico_City'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'es-mx'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = '/media/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = os.path.join(BASE_DIR, 'static_collection')

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'

# Additional locations of static files
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static_files'),
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    # 'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'ld2r@o#od)p9c+)l)jv3!!!b8n2=jmfq8edj&amp;tz&amp;%ok(qt*8po'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    # 'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'core.middleware.LoginRequiredMiddleware',
    'core.middleware.ForceProfileMiddleware',
    'core.middleware.MembershipValidatorMiddleware',
    'async_messages.middleware.AsyncMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'nube_inmobiliaria.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'nube_inmobiliaria.wsgi.application'

TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, '..', 'templates'),
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'django.contrib.humanize',
    'django.contrib.sites',
    'django.contrib.flatpages',
    'django.contrib.markup',
    'registration',
    'core',
    'contact_form',
    'properties',
    'sepomex',
    'tastypie',
    'sorl.thumbnail',
    'currencies',
    'ads',
    'paypal.standard.ipn',
)

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}


YOUTUBE_SRC = "http://www.youtube.com/embed/8hyIHy1fNZU"

AUTH_PROFILE_MODULE = 'core.Profile'
ACCOUNT_ACTIVATION_DAYS = 7
LOGIN_REDIRECT_URL = '/perfil/'
LOGIN_URL = '/cuentas/login/'

DEFAULT_IMAGE_URL = STATIC_URL + 'images/ui/default_profile.png'


if DEBUG:
    PAYPAL_RECEIVER_EMAIL = "seller_1355779416_biz@robles.ws"
else:
    pass

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.db.DatabaseCache',
        'LOCATION': 'cache',
    }
}

LOGIN_EXEMPT_URLS = (
    r'^$',  # allow the entire /r/* all auth
    r'^cuentas/',
    r'^contacto/',
    r'^acerca-de/$',
    r'^FAQ/$',
    r'paypal/',
    r'^pagina/',
)

# get local settings if set
try:
    ONCE
except NameError:
    try:
        from local_settings import *
    except ImportError:
        pass
