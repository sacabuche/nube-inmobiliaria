from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
    (r'^pagina/', include('django.contrib.flatpages.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^', include('core.urls')),
    url(r'^inmuebles/', include('properties.urls')),
    url(r'^contacto/', include('contact_form.urls')),
    url(r'^cuentas/logout/', 'django.contrib.auth.views.logout',
        {'next_page': '/'}, name='logout'),
    url(r'^cuentas/', include('django.contrib.auth.urls')),
    url(r'^cuentas/', include('registration.backends.email_terms.urls')),
    url(r'^api/', include('sepomex.api.urls')),
    url(r'^ads/', include('ads.api.urls')),
)


if settings.DEBUG:
    if 'rosetta' in settings.INSTALLED_APPS:
        urlpatterns += (url(r'^rosetta/', include('rosetta.urls')),)
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
