# coding: utf-8
from datetime import datetime
import random
import string

from django.db import models
from django.contrib.auth.models import User
from django.conf import settings
from django.core.exceptions import ValidationError

NOW = datetime.now()
CURRENT_YEAR = NOW.year
YEAR_DIFF = 100
REF_ID_SIZE = 6
PHONE_MIN = 10
PHONE_MAX = 18


def validate_phone(phone, exception=ValidationError):
    phone = phone.replace(' ', '').replace('(', '').replace(')', '')
    try:
        int(phone)
    except (ValueError, TypeError):
        raise ValidationError('Solo acepta números'
                              ', espacios y paréntesis')
    if len(phone) < PHONE_MIN:
        raise ValidationError('El minimo de'
                              ' digitos es de %s' % PHONE_MIN)
    if len(phone) > PHONE_MAX:
        raise ValidationError('El máximo de'
                              ' digitos es de %s' % PHONE_MAX)
    return phone


class NormalProfile(models.Model):

    YEARS = range(CURRENT_YEAR, CURRENT_YEAR - YEAR_DIFF, -1)
    YEAR_CHOICES = [(year, year) for year in YEARS]
    YEAR_CHOICES.insert(0, (None, '-----'))

    user = models.OneToOneField(User, editable=False)
    working_since = models.IntegerField('trabajando desde',
                                        choices=YEAR_CHOICES,
                                        default=CURRENT_YEAR,
                                        help_text='¿En que año empezo a '
                                        'trabajar como asesor inmobiliario?',
                                        null=True)
    phone1 = models.CharField('Teléfono de Celular',
                              max_length=20)
    phone2 = models.CharField('Teléfono de Oficina o casa',
                              max_length=20, blank=True)
    address = models.TextField('Dirección')
    AMPI = models.CharField('Entidad que certifica', max_length=100,
                            blank=True,
                            help_text='Sí esta certificado, mencione'
                            ' la entidad. Ej, AMPI')
    ref1_name = models.CharField('Nombre de referencia 1', max_length=120)
    ref1_number = models.CharField('Teléfono de referencia 1', max_length=20)
    ref2_name = models.CharField('Nombre de referencia 2', max_length=120)
    ref2_number = models.CharField('Teléfono de referencia 2', max_length=20)

    # TODO: Create a Field to do This
    _phone_fields = ('phone1', 'phone2', 'ref1_number', 'ref2_number')

    @property
    def experience(self):
        try:
            return CURRENT_YEAR - self.working_since
        except TypeError:
            pass


class VIPProfile(NormalProfile):
    pass


class DevProfile(models.Model):
    company_name = models.CharField('nombre de la empresa', max_length=120)
    RFC = models.CharField('RFC', max_length=20)
    address = models.TextField('Dirección de la empresa')
    phone = models.CharField('Teléfono', max_length=20, blank=True)


class Profile(models.Model):

    NORMAL = 0
    VIP = 1
    DEV = 2

    CLS_PROFILES = {
        NORMAL: NormalProfile,
        VIP: VIPProfile,
        DEV: DevProfile,
    }

    user = models.OneToOneField(User, editable=False)
    user_type = models.SmallIntegerField('tipo de usuario', editable=False,
                                         default=NORMAL)
    image = models.ImageField('foto', upload_to='profile_images', null=True)
    ref_id = models.CharField(max_length=10, editable=False, unique=True)

    image_default_url = settings.DEFAULT_IMAGE_URL

    def gen_ref_id(self):
        selectable = string.digits
        digits = (random.choice(selectable) for x in range(REF_ID_SIZE))
        return ''.join(digits)

    def _new_ref_id(self):
        for x in range(1000):
            ref_id = self.gen_ref_id()
            try:
                type(self).objects.get(ref_id=ref_id)
            except self.DoesNotExist:
                break
        else:
            raise Exception('Problem Creating new id')
        self.ref_id = ref_id

    def save(self, *args, **kwargs):
        if not self.id:
            self._new_ref_id()
        return super(Profile, self).save(*args, **kwargs)

    @property
    def cls(self):
        attr = '__cls_profile'
        try:
            return getattr(self, attr)
        except AttributeError:
            pass
        setattr(self, attr, self.CLS_PROFILES[self.user_type])
        return self.cls

    def get(self):
        cls_profile = self.cls
        return cls_profile.objects.get(user__id=self.user_id)

    @property
    def form_cls(self):
        from forms import TYPE_FORMS
        return TYPE_FORMS[self.user_type]

    @property
    def image_url(self):
        if self.image:
            return self.image.url
        return self.image_default_url


class Document(models.Model):
    doc = models.FileField(upload_to='docs/%Y/%m')
    name = models.CharField('nombre', max_length=200, unique=True)

    def __unicode__(self):
        return self.name


class FAQ(models.Model):
    question = models.TextField('pregunta')
    answer = models.TextField('respuesta')

    def __unicode__(self):
        return self.question
