from django.core.exceptions import ObjectDoesNotExist
from django import template
from django.conf import settings

register = template.Library()


class Profile(object):

    def __init__(self, user):
        self._profile = user.get_profile()
        self._info = self._profile.get()

    def __getattr__(self, name):
        try:
            return getattr(self._profile, name)
        except AttributeError:
            pass

        try:
            return getattr(self._info, name)
        except AttributeError:
            raise


@register.assignment_tag
def get_profile(user):
    try:
        return Profile(user)
    except ObjectDoesNotExist:
        return None


@register.simple_tag
def default_profile_image():
    return settings.DEFAULT_IMAGE_URL
