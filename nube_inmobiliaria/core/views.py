from datetime import datetime

from django.views.decorators.csrf import csrf_exempt
from django.core.exceptions import ImproperlyConfigured
from django.contrib.sites.models import get_current_site
from django.views.generic import TemplateView, ListView, View, DetailView
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.shortcuts import redirect
from django.conf import settings

from core.models import Profile, Document, FAQ
from core.forms import ProfileForm, UserForm

from ads.models import BaseAd, BaseMembership

from paypal.standard.forms import PayPalPaymentsForm


class HomeView(TemplateView):
    template_name = 'core/index.html'

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            return redirect('profile-home')
        return super(HomeView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        context['youtube_src'] = settings.YOUTUBE_SRC
        return context


class AboutView(TemplateView):
    template_name = 'core/about.html'


class FaqView(ListView):
    model = FAQ
    template_name = 'core/faq.html'


class HomeProfileView(View):

    def get(self, request):
        return redirect('property-search')


class EditProfileView(TemplateView):
    template_name = 'core/profile/edit.html'
    user_prefix = 'user'
    profile_prefix = 'profile'
    all_info_prefix = 'all_info'

    def set_profiles(self):
        user = self.request.user
        # get or create profile
        try:
            profile = user.get_profile()
        except ObjectDoesNotExist:
            profile = Profile.objects.create(user=user)

        # get real profile that depends on the type of Profile
        try:
            all_info = profile.get()
        except ObjectDoesNotExist:
            all_info = profile.cls(user=user)
        self.profile = profile
        self.all_info = all_info

    def create_forms(self):
        self.set_profiles()
        user_form = UserForm(instance=self.request.user,
                             prefix=self.user_prefix)
        profile_form = ProfileForm(instance=self.profile,
                                   prefix=self.profile_prefix)
        all_info_form = self.profile.form_cls(instance=self.all_info,
                                              prefix=self.all_info_prefix)
        self.forms = {
            'user': user_form,
            'image': profile_form,
            'profile': all_info_form
        }

    def get_context_data(self, **kwargs):
        context = super(EditProfileView, self).get_context_data(**kwargs)
        context['profile'] = self.profile
        context['all_info'] = self.all_info
        context['forms'] = self.forms
        return context

    def get(self, *args, **kwargs):
        self.create_forms()
        return super(EditProfileView, self).get(*args, **kwargs)

    def post(self, *args, **kwargs):
        request = self.request
        self.set_profiles()
        user_form = UserForm(request.POST,
                             instance=request.user,
                             prefix=self.user_prefix)
        profile_form = ProfileForm(request.POST, request.FILES,
                                   instance=self.profile,
                                   prefix=self.profile_prefix)
        all_info_form = self.profile.form_cls(request.POST,
                                              instance=self.all_info,
                                              prefix=self.all_info_prefix)
        self.forms = {
            'user': user_form,
            'image': profile_form,
            'profile': all_info_form
        }
        # check if all forms are valid
        forms = (user_form, profile_form, all_info_form)
        if all(getattr(f, 'is_valid')() for f in forms):
            user_form.save()
            profile_form.save()
            all_info_form.save()
            return redirect('profile-home')
        context = self.get_context_data()
        return self.render_to_response(context)


class DocumentsProfileView(TemplateView):
    template_name = 'core/profile/documents.html'

    def get(self, request):
        documents = Document.objects.extra(select={
            'lower_name': 'lower(name)'
        })

        return self.render_to_response({
            'documents': documents.order_by('lower_name'),
        })


class MembersView(ListView):
    queryset = User.objects.filter(is_active=True).\
        exclude(first_name='').\
        exclude(last_name='').\
        exclude(profile__isnull=True)
    context_object_name = 'members'
    template_name = 'core/profile/members.html'


class MemberDetailView(DetailView):
    queryset = User.objects.filter(is_active=True).\
        exclude(first_name='').\
        exclude(last_name='').\
        exclude(profile__isnull=True)
    context_object_name = 'member'
    template_name = 'core/profile/member_detail.html'


class OwnAccountView(TemplateView):
    template_name = 'core/profile/own_account.html'


class FavouritesView(ListView):
    template_name = 'core/profile/favorites.html'

    def get_queryset(self):
        return self.request.user.favourites.all()


class SelectAdView(TemplateView):
    template_name = 'core/profile/ad_select.html'

    def post(self, request):
        ad_id = request.POST.get('ad-id', 0)
        return redirect('profile-pay', pk=ad_id)


class PaymentMixin(object):
    key = None
    url_base = 'http://{domain}{path}'
    price_attr = 'price'

    def get_invoice(self):
        date = datetime.now().strftime('%S%y%j%H%M')
        if not self.key:
            raise ImproperlyConfigured('key attribute must be set')
        invoice = map(unicode, (self.key, self.request.user.id,
                                self.object.id, date))
        return '-'.join(invoice)

    def url(self, domain, path):
        return self.url_base.format(domain=domain, path=path)

    def get_form(self):
        amount = getattr(self.object, self.price_attr, None)
        if not amount:
            raise ImproperlyConfigured('object must have price attribute')

        domain = get_current_site(self.request).domain
        paypal_dict = {
            "business": settings.PAYPAL_RECEIVER_EMAIL,
            "amount": round(amount, 2),
            "item_name": unicode(self.object),
            "invoice": self.get_invoice(),
            "currency_code": "MXN",
            "notify_url": self.url(domain, reverse('paypal-ipn')),
            "return_url": self.url(domain, reverse('profile-ad-buyed')),
            "cancel_return": self.url(domain, reverse('profile-ad-canceled')),
        }
        form = PayPalPaymentsForm(initial=paypal_dict)
        if settings.DEBUG:
            return form.sandbox()
        else:
            return form.render()


class PayAdView(PaymentMixin, DetailView):
    model = BaseAd
    template_name = 'core/profile/pay_ad.html'
    key = 'ad'
    price_attr = 'total'

    def get_context_data(self, **kwargs):
        context = super(PayAdView, self).get_context_data(**kwargs)
        context['form'] = self.get_form()
        return context


class MemebershipSelectView(ListView):
    model = BaseMembership
    template_name = 'core/profile/select_membership.html'


class MembershipPayView(PaymentMixin, DetailView):
    model = BaseMembership
    template_name = 'core/profile/pay_membership.html'
    key = 'me'

    def get_context_data(self, **kwargs):
        context = super(MembershipPayView, self).get_context_data(**kwargs)
        context['form'] = self.get_form()
        return context


class AdBuyedView(TemplateView):
    template_name = 'core/profile/ad_buyed.html'

    def post(self, *args, **kwargs):
        return self.get(*args, **kwargs)


class AdNotBuyedView(TemplateView):
    template_name = 'core/profile/ad_not_buyed.html'


home_profile_view = HomeProfileView.as_view()
edit_profile_view = EditProfileView.as_view()
documents_profile_view = DocumentsProfileView.as_view()
members_view = MembersView.as_view()
member_detail_view = MemberDetailView.as_view()
account_view = OwnAccountView.as_view()
favourites_view = FavouritesView.as_view()
buy_ad_view = SelectAdView.as_view()
pay_ad_view = PayAdView.as_view()
ad_buyed_view = csrf_exempt(AdBuyedView.as_view())
ad_not_buyed_view = AdNotBuyedView.as_view()
membership_select_view = MemebershipSelectView.as_view()
membership_pay_view = MembershipPayView.as_view()
