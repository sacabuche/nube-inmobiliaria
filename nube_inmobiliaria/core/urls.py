from django.conf.urls.defaults import patterns, url, include
from properties.views import user_properties_view
from core.views import HomeView, AboutView, FaqView
from core.views import (home_profile_view, members_view,
                        documents_profile_view,
                        edit_profile_view, favourites_view,
                        buy_ad_view, pay_ad_view, ad_buyed_view,
                        ad_not_buyed_view, membership_select_view,
                        membership_pay_view, member_detail_view)


URLS = (
    url(r'^$', HomeView.as_view(), name='home'),

    url(r'^acerca-de/$', AboutView.as_view(), name='about'),

    url(r'^FAQ/$', FaqView.as_view(), name='faq'),

    url(r'^perfil/$', home_profile_view, name='profile-home'),

    url(r'^perfil/mis-propiedades/$', user_properties_view,
        name='profile-properties'),

    url(r'^perfil/documentacion/$', documents_profile_view,
        name='profile-documents'),

    url(r'^perfil/editar/$', edit_profile_view, name='profile-edit'),

    url(r'^perfil/miembros/$', members_view, name='profile-members'),
    url(r'^perfil/miembros/(?P<pk>\d+)/$', member_detail_view,
        name='profile-member-detail'),
    url(r'^perfil/cuenta/$', user_properties_view, name='profile-account'),

    url(r'^perfil/favoritos/$', favourites_view, name='profile-favourites'),

    # select, buy ads
    url(r'^perfil/comprar/$', buy_ad_view, name='profile-buy'),
    url(r'^perfil/pagar/(?P<pk>\d+)/$', pay_ad_view, name='profile-pay'),

    # select, buy memberships
    url(r'^perfil/selecionar-membresia/$', membership_select_view,
        name='profile-select-membership'),
    url(r'^perfil/pagar-membresia/(?P<pk>\d+)/$', membership_pay_view,
        name='profile-pay-membership'),

    # result of payment
    url(r'^perfil/compra-realizada/$', ad_buyed_view, name='profile-ad-buyed'),
    url(r'^perfil/compra-cancelada/$', ad_not_buyed_view,
        name='profile-ad-canceled'),

    (r'^paypal/r-ds/dsfe/gpsd290-+sd/r/ef26/',
     include('paypal.standard.ipn.urls')),
)

urlpatterns = patterns('', *URLS)
