from django.forms import ModelForm, ValidationError
from django.contrib.auth.models import User
from core.models import Profile, NormalProfile, validate_phone


class UserForm(ModelForm):
    class Meta:
        model = User
        fields = ('first_name', 'last_name')


class ProfileForm(ModelForm):
    class Meta:
        model = Profile
        fields = ('image',)


def wrapp(f, name):
    def w():
        return f(name)
    return w


class NormalProfileForm(ModelForm):

    _clean_names = ['clean_' + field for field in NormalProfile._phone_fields]

    class Meta:
        model = NormalProfile

    def __getattr__(self, name):
        if name in self._clean_names:
            return wrapp(self._clean_phone, name)
        raise AttributeError

    # TODO: Replace by a Phone Field on NormalProfile
    def _clean_phone(self, clean_method_name):
        name = clean_method_name.replace('clean_', '', True)
        phone = self.cleaned_data[name]
        phone = validate_phone(phone, ValidationError)
        return phone


TYPE_FORMS = {
    Profile.NORMAL: NormalProfileForm,
}
