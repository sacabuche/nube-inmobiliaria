# coding: utf-8
import re

from django.http import HttpResponseRedirect
from django.conf import settings
from django.utils import timezone
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse, resolve
from django.contrib import messages

from ads.models import Membership

LOGIN_URL = settings.LOGIN_URL
LOGIN_EXEMPT_URLS = [re.compile(LOGIN_URL.lstrip('/'))]
if hasattr(settings, 'LOGIN_EXEMPT_URLS'):
    LOGIN_EXEMPT_URLS += [re.compile(expr)
                          for expr in settings.LOGIN_EXEMPT_URLS]


class LoginRequiredMiddleware:
    """
    Middleware that requires a user to be authenticated to view any page other
    than LOGIN_URL. Exemptions to this requirement can optionally be specified
    in settings via a list of regular expressions in LOGIN_EXEMPT_URLS (which
    you can copy from your urls.py).

    Requires authentication middleware and template context processors to be
    loaded. You'll get an error if they aren't.
    """
    def process_request(self, request):
        assert hasattr(request, 'user'), "The Login Required middleware "\
            "requires authentication middleware to be installed. "\
            "Edit your MIDDLEWARE_CLASSES setting to insert "\
            "'django.contrib.auth.middlware.AuthenticationMiddleware'. "\
            "If that doesn't work, ensure your "\
            "TEMPLATE_CONTEXT_PROCESSORS setting includes "\
            "'django.core.context_processors.auth'."
        if not request.user.is_authenticated():
            path = request.path_info.lstrip('/')
            if not any(m.match(path) for m in LOGIN_EXEMPT_URLS):
                login_url = ''.join((LOGIN_URL, '?next=', request.path))
                return HttpResponseRedirect(login_url)


class ForceProfileMiddleware:

    def process_request(self, request):
        user = request.user
        if not user.is_authenticated():
            return

        names = ('profile-edit',
                 'logout')

        resolver = resolve(request.path)
        if resolver.url_name in names:
            return

        has_profile = True
        try:
            user.get_profile().get()
        except ObjectDoesNotExist:
            has_profile = False

        # if has first_name and last_name just continue
        if has_profile and user.get_full_name().strip():
            return

        messages.add_message(request, messages.INFO, 'Para continuar '
                             'necesita llenar su nombre completo y perfil')
        return HttpResponseRedirect(reverse('profile-edit'))


class MembershipValidatorMiddleware:

    def process_request(self, request):
        user = request.user
        if not user.is_authenticated() or user.is_staff or user.is_superuser:
            return

        names = (
            'logout',
            'profile-select-membership',
            'profile-pay-membership'
        )

        resolver = resolve(request.path)
        if resolver.url_name in names:
            return

        try:
            user.membership_set.get(valid_until__gt=timezone.now())
            return
        except Membership.DoesNotExist:
            pass
        message = u'Su membresía ha expirado'
        messages.add_message(request, messages.WARNING, message)
        return HttpResponseRedirect(reverse('profile-select-membership'))
