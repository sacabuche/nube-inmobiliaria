/*global _:true*/

$(document).ready(function () {
    var ads_by_month,
    no_ads_list,
    $ads = $('#ads'),
    $months = $('#months'),
    $price = $('#price'),
    $id = $('#ad-id'),
    $tax = $('#tax'),
    $total = $('#total');


    function ordering(data) {
        ads_by_month = _.chain(data.objects)
        .sortBy(function (ad) {
            return ad.number_of_ads;
        }).groupBy('number_of_ads').value();
        no_ads_list = _.chain(ads_by_month)
            .keys()
            .map(function (value) {
                return parseInt(value, 10);
            }).sortBy(function (num) {
                return num;
            }).value();
    }

    function init(data) {
        ordering(data);
        var ad = find(1, 1);
        set_ad(ad);
    }

    function change(add_ad, add_month) {
        var no_ads = parseInt($ads.html(), 10),
        no_months = parseInt($months.html(), 10),
        ad;

        add_ad = add_ad || 0;
        add_month = add_month || 0;
        no_ads = _.isNumber(no_ads) ? no_ads : 1;
        no_months = _.isNumber(no_months) ? no_months : 1;
        ad = find(no_ads + add_ad, no_months + add_month,
                  add_ad > 0 ? true: false,
                  add_month > 0 ? true: false);
        set_ad(ad);
    }

    function set_ad(ad) {
        if (!ad) {
            return;
        }
        var price = parseFloat(ad.price, 10);
        var tax = price * 0.16;
        $ads.html(ad.number_of_ads);
        $months.html(ad.months);
        $price.html(price.toFixed(2));
        $tax.html(tax.toFixed(2));
        $total.html('$ ' + (price + tax).toFixed(2));
        $id.prop('value', ad.id);
    }

    function find(no_ads, no_months, ads_up, months_up) {
        var ads = ads_by_month[no_ads];
        var numbers = no_ads_list.slice();
        var compare;

        function up_compare(current, searching) {
            return current >= searching;
        }

        function down_compare(current, searching) {
            return current <= searching;
        }

        function cycle(up, list) {
            if (up) {
                return _.first(list);
            }
            return _.last(list);
        }

        /* select if up or down compare for ads */
        if (ads_up) {
            compare = up_compare;
        }
        else {
            numbers.reverse();
            compare = down_compare;
        }

        if (!ads) {
            no_ads = _.find(numbers, function (num) {
                return compare(num, no_ads);
            });
            no_ads = no_ads || cycle(ads_up, no_ads_list);
            ads = ads_by_month[no_ads];
            if (!ads) {
                return;
            }
        }

        if (months_up) {
            compare = up_compare;
        }
        else {
            compare = down_compare;
        }

        var ad = _.find(ads, function (obj) {
            return compare(obj.months, no_months);
        });
        ad = ad || cycle(months_up, ads);
        return ad;
    }

    $('#add-ad').click(function () {
        change(1, 0);
    });
    $('#add-month').click(function () {
        change(0, 1);
    });
    $('#less-ad').click(function () {
        change(-1, 0);
    });
    $('#less-month').click(function () {
        change(0, -1);
    });
    $.get('/ads/api/v1/base_ad/', init);

});
