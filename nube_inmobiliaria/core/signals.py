# coding: utf-8
from datetime import datetime

from registration.signals import user_activated
from django.contrib.messages import constants as messages
from django.contrib.auth.models import User
from django.dispatch import receiver
from django.conf import settings
from django.core.mail import send_mail

from async_messages import message_user

from paypal.standard.ipn.signals import (payment_was_successful,
                                         payment_was_flagged)
from ads.models import BaseAd, BaseMembership


FIRST_MEMBERSHIP_DAYS = getattr(settings, 'FIRST_MEMBERSHIP_DAYS', 30 * 2)


SUBJECT_OK = 'Gracias por su compra [{invoice}]'
SUBJECT_ERROR = 'Problema con su compra [{invoice}]'
MESSAGE_OK = """
Hola {name},

Ya esta disponible su compra:
{obj}


Atentamente
Administrador de Nube Inmobiliaria
"""

MESSAGE_ERROR = """
Hola {name},

Por alguna razón su compra no ha podido ser completada,
su cuenta de paypal reflegará mas información, sí tiene
cualquier duda puede escribirnos a:
    contacto@nubeinmobiliaria.com.mx

Sentimos las molestias


=== Informacion Importante ===
numero de envio: {invoice}
"""


@receiver(payment_was_successful)
def payment_succesful(sender, **kwargs):
    invoice = sender.invoice.split('-')
    # validate date format
    datetime.strptime(invoice[-1], '%S%y%j%H%M')
    user = User.objects.get(id=invoice[1])
    # TODO Make this a litle more generic
    invoice_type = invoice[0]
    if invoice_type == 'ad':
        obj = BaseAd.objects.get(id=invoice[2])
        obj.gen_ads(user)
    elif invoice_type == 'me':
        obj = BaseMembership.objects.get(id=invoice[2])
        obj.gen(user)

    send_mail(SUBJECT_OK.format(invoice=sender.invoice),
              MESSAGE_OK.format(name=user.get_full_name(), obj=obj),
              settings.DEFAULT_FROM_EMAIL,
              (user.email,))
    message_user(user, 'Su compra ha sido confirmada, ya puede usar'
                 ' su espacio',
                 messages.SUCCESS)


# With out this the user has to buy his first membership
@receiver(user_activated)
def make_first_membership(sender, **kwargs):
    user = kwargs['user']
    bm = BaseMembership.objects.all().order_by('id')[0]
    bm.gen(user, days=FIRST_MEMBERSHIP_DAYS)


@receiver(payment_was_flagged)
def payment_flagged(sender, **kwargs):
    invoice = sender.invoice.split('-')
    user = User.objects.get(id=invoice[1])
    send_mail(SUBJECT_ERROR.format(invoice=sender.invoice),
              MESSAGE_ERROR.format(name=user.get_full_name(),
                                   invoice=sender.invoice),
              settings.DEFAULT_FROM_EMAIL,
              (user.email,))
