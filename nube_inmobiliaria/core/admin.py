from django.contrib import admin
from models import Document, FAQ


admin.site.register(Document)
admin.site.register(FAQ)
