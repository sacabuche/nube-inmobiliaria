<?php
	require_once "HTML/Template/ITX.php";
	
	
	// ========================================================================
	//
	// 	Cargamos el template y desplegamos la pagina inicial 
	// 	de la configuracion de la empresa
	// 
	// ========================================================================
	$template = new HTML_Template_ITX('../templates');
	$template->loadTemplatefile("mainTemplate.html", true, true);
	
	$location = $cfgLocation['location'];
	
	$template->setvariable("TITULO_ADICIONAL",$tituloSede);
	
	$template->setvariable("TITULO_CONTENIDO",$tituloItinerario);
	
	$template->setVariable("CLASE_WRAP", "wrapContenido");
	
	$template->setVariable("CLASE_MENU", "menu");
	
	$template->setVariable("CLASE_LOGIN", "caja_login");
	
	$template->setvariable("CLASE_CONTENIDO","contenido");
	$template->setvariable("ID_CONTENIDO","contenido");
	
	$template->setvariable("CLASE_ADICIONAL","adicional");
	
	$template->setvariable("CLASE_ENCABEZADO","registro_contacto");
	
	$template->addBlockFile('ENCABEZADO', 'ENCABEZADO_MAIN', './encabezado/encabezado.html');
	$template->setCurrentBlock("ENCABEZADO_MAIN");
	$template->setVariable("LEGALES","LEGALES");
	$template->setVariable("ACERCA_DE","ACERCA DE");
	$template->setVariable("POLITICAS","POL&Iacute;TICAS");
	$template->setVariable("FAQ","FAQ");
	$template->setVariable("CONTACTO","CONTACTO");
	$template->parseCurrentBlock('ENCABEZADO_MAIN');

	
	$template->addBlockFile('LOGIN', 'LOGIN_MAIN', './login/login.html');
	$template->setCurrentBlock("LOGIN_MAIN");	
	$template->setVariable("TITULO_LOGIN", "Usuarios Registrados");
	$template->setVariable("USUARIO", "Usuario");
	$template->setVariable("PASSWORD", "Contrase&ntilde;a");
	$template->parseCurrentBlock('LOGIN_MAIN');
	/*
	$template->addBlockFile('MENU', 'MENU_MAIN', './menu/menu.html');
	$template->setCurrentBlock("MENU_MAIN");
	$template->setVariable("IDIOMA", $idioma);
	$template->parseCurrentBlock('MENU_MAIN');
	
	$template->addBlockFile('ADICIONAL', 'SEDES_MAIN', './sedes/sedes.html');
	$template->setCurrentBlock("SEDES_MAIN");
	$template->touchBlock('SEDES_MAIN');
	
	$template->addBlockFile('SCRIPT_LOAD', 'SCRIPT_ONLOAD', './login/script.html');
	$template->setCurrentBlock("SCRIPT_ONLOAD");
	$template->setVariable("LOCATION", $location);
	$template->setVariable("IDIOMA_SCRIPT", $idioma);
	$template->setVariable("ERROR_ENTRADA", $errorEntrada);
	$template->parseCurrentBlock('SCRIPT_ONLOAD');
	
	
	// Agregamos el contenido principal de la pagina
	$template->addBlockfile("CONTENIDO", "PROGRAMA", "./itinerario/itinerario.html");
	$template->setCurrentBlock("PROGRAMA");	
	$template->touchBlock('PROGRAMA');	
*/
	$template->show();
	
?>
