<?php
	require_once "HTML/Template/ITX.php";
		
	// ========================================================================
	//
	// 	Cargamos el template y desplegamos la pagina inicial 
	// 	de la configuracion de la empresa
	// 
	// ========================================================================
	$template = new HTML_Template_ITX('../templates');
	$template->loadTemplatefile("mainTemplate.html", true, true);
	
	$template->setVariable("CLASE_WRAP", "wrapContenido");
	
	$template->setVariable("CLASE_MENU", "menu");
	
	$template->setVariable("CLASE_LOGIN", "caja_login");
	
	$template->setvariable("CLASE_ENCABEZADO","registro_contacto");
	
	$template->setvariable("CLASE_CONTENIDO","contenido");
	$template->setvariable("ID_CONTENIDO","contenido");
	
	$template->addBlockFile('ENCABEZADO', 'ENCABEZADO_MAIN', './encabezado/encabezado_politicas.html');
	$template->setCurrentBlock("ENCABEZADO_MAIN");
	$template->setVariable("LEGALES","LEGALES");
	$template->setVariable("ACERCA_DE","ACERCA DE");
	$template->setVariable("POLITICAS","POL&Iacute;TICAS");
	$template->setVariable("FAQ","FAQ");
	$template->setVariable("CONTACTO","CONTACTO");
	$template->parseCurrentBlock('ENCABEZADO_MAIN');
	
	
	$template->addBlockFile('LOGIN', 'LOGIN_MAIN', './login/login.html');
	$template->setCurrentBlock("LOGIN_MAIN");
	$template->setVariable("TITULO_LOGIN", "Usuarios Registrados");
	$template->setVariable("USUARIO", "Usuario");
	$template->setVariable("PASSWORD", "Contrase&ntilde;a");
	$template->parseCurrentBlock('LOGIN_MAIN');
	
	/*
	$template->addBlockFile('MENU', 'MENU_MAIN', './menu/menu_registro.html');
	$template->setCurrentBlock("MENU_MAIN");
	$template->setVariable("IDIOMA", $idioma);
	$template->parseCurrentBlock('MENU_MAIN');
	*/
	
	// Agregamos el contenido principal de la pagina
	$template->addBlockfile("CONTENIDO", "TERMINOS_Y_CONDICIONES", "./terminos/terminosYCondiciones.html");
	$template->setCurrentBlock("TERMINOS_Y_CONDICIONES");	
	$template->touchBlock('TERMINOS_Y_CONDICIONES');	

	$template->show();
	
?>
